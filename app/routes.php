<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

/* Início
 * Rotas do ADMIN
 */
// Autenticados
Route::group(array('prefix' => 'admin', 'before' => 'adminAuth|permissionAccessRoute'), function() {
    Route::post('categoria-projetos/listagem', "admin\ProjectCategoriesController@postListing");
    Route::post('categoria-projetos/salvar', "admin\ProjectCategoriesController@postSave");
    Route::post('categoria-projetos/excluir/{id}', "admin\ProjectCategoriesController@postDelete");
    Route::get('categoria-projetos/editar/{id}', "admin\ProjectCategoriesController@getEdit");
    Route::get('categoria-projetos/novo', "admin\ProjectCategoriesController@getNew");
    Route::get('categoria-projetos', "admin\ProjectCategoriesController@getIndex");
    
    Route::post('usuarios/listagem', "admin\UserController@postListing");
    Route::post('usuarios/salvar', "admin\UserController@postSave");
    Route::post('usuarios/excluir/{id}', "admin\UserController@postDelete");
    Route::get('usuarios/editar/{id}', "admin\UserController@getEdit");
    Route::get('usuarios/novo', "admin\UserController@getNew");
    Route::get('usuarios', "admin\UserController@getIndex");

    Route::post('projetos/listagem', "admin\RealEstateController@postListing");
    Route::post('projetos/salvar', "admin\RealEstateController@postSave");
    Route::post('projetos/salvar-alteracoes/{id}', "admin\RealEstateController@postSaveUpdates");
    Route::post('projetos/excluir-imagem/{id}', "admin\RealEstateController@postDeleteImage");
    Route::post('projetos/excluir-investimento/{id}', "admin\RealEstateController@postDeleteInvestment");
    Route::post('projetos/excluir/{id}', "admin\RealEstateController@postDelete");
    Route::get('projetos/editar/{id}', "admin\RealEstateController@getEdit");
    Route::get('projetos/novo', "admin\RealEstateController@getNew");
    Route::get('projetos', "admin\RealEstateController@getIndex");

    Route::post('home/excluir-banner/{id}', "admin\HomeController@postDeleteBanner");
    Route::post('home/salvar', "admin\HomeController@postSave");
    Route::get('home', "admin\HomeController@getIndex");

    Route::post('empresas-grupo/editar/{id}', "admin\GroupsCompaniesController@edit");
    Route::post('empresas-grupo/excluir/{id}', "admin\GroupsCompaniesController@delete");
    Route::post('empresas-grupo/salvar', "admin\GroupsCompaniesController@save");
    Route::get('empresas-grupo', "admin\GroupsCompaniesController@index");

    Route::post('principais-clientes/editar/{id}', "admin\ClientsController@edit");
    Route::post('principais-clientes/excluir/{id}', "admin\ClientsController@delete");
    Route::post('principais-clientes/salvar', "admin\ClientsController@save");
    Route::get('principais-clientes', "admin\ClientsController@index");

    Route::post('sobre-a-empresa/salvar', "admin\AboutTheCompanyController@postSave");
    Route::get('sobre-a-empresa', "admin\AboutTheCompanyController@getIndex");
    Route::post('politica-de-privacidade/salvar', "admin\PrivacyPolicyController@postSave");
    Route::get('politica-de-privacidade', "admin\PrivacyPolicyController@getIndex");

    Route::get('logout', "admin\LoginController@getlogout");
});
// Não Autenticados
Route::group(array('prefix' => 'admin', 'before' => 'adminNotAuth'), function() {
    Route::get('login', "admin\LoginController@getIndex");
    Route::post('login', "admin\LoginController@postIndex");
    Route::post('enviar-senha', "admin\LoginController@postSendPassword");
    Route::get('esqueci-senha', "admin\LoginController@getForgotPassword");
});

Route::get('admin', "admin\IndexController@getIndex");
/* Fim
 * Rotas do ADMIN
 */


Route::get('politica-de-privacidade/download', function () {
    return \Response::download('assets/politica-privacidade.pdf');
});


/* Início
 * Rotas do SITE
 */
Route::get('projetos/detalhes/{id}', "site\RealEstateController@getDetail");
Route::get('projetos', "site\RealEstateController@getIndex");
Route::get('politica-privacidade', "site\PrivacyPolicyController@getIndex");
Route::get('quem-somos', "site\WhoWeAreController@getIndex");
Route::post('portal-do-cliente/enviar', "site\CustomerPortalController@postSend");
Route::get('portal-do-cliente', "site\CustomerPortalController@getIndex");
Route::post('contato/enviar', "site\ContactController@postSend");
Route::get('contato', "site\ContactController@getIndex");

Route::get('/', "site\IndexController@getIndex");
