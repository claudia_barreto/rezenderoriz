<?php

View::composer('partials.admin.pageheader', function($view) {
    $array_route = array('novo' => 'Novo', 'editar' => 'Editar', 'permissoes' => 'Permissão', 'galeria' => 'Galeria', 'imagens' => 'Imagens', 'facturas' => 'Facturas');
    $route1 = Request::segment(2);
    $rota2 = Request::segment(3);
    $rota3 = Request::segment(4);
    $title = [];
    $icon = '';
    if (empty($route1)) {
        $title[] = "Página inicial";
        $icon = "fa-home";
    } else if ($route1 == "perfil") {
        $title[] = "Perfil";
        $icon = "fa-user";
    } else if ($route1 == "support") {
        $title[] = "Suporte";
        $icon = "fa-info-circle";
    } else {
        $rota = \Menu::title($route1);
        if (isset($rota[0])) {
            $title[] = $rota[0]['name'];
            $icon = $rota[0]['icon'];
            if (isset($rota[0]['sub_menus'][0]) && count($rota[0]['sub_menus']) > 0) {
                $title[] = $rota[0]['sub_menus'][0]['name'];
                if (!empty($rota2)) {
                    $title[] = ucwords((isset($array_route[$rota2]) ? $array_route[$rota2] : (isset($array_route[$rota3]) ? $array_route[$rota3] : '')));
                }
            }
        }
    }

    $view->with('icon', $icon);
    $view->with('subtitle', ucwords((isset($title[1]) ? $title[1] . ((isset($array_route[$rota2])) ? ' / ' . $array_route[$rota2] : ((isset($array_route[$rota3])) ? ' / ' . $array_route[$rota3] : '')) : $title[0])));
    $view->with('title', implode(' / ', $title));
});

View::composer('partials.admin.leftpanel', function($view) {
    $route = Request::segment(2);
    $query = \Menu::menus();

    $active = [];
    foreach ($query as $value) {
        foreach ($value['sub_menus'] as $v) {
            if ($v['route'] == $route) {
                $active[$value['id']] = 'active';
                break;
            }
        }
    }

    $view->with('route', $route);
    $view->with('menu_active', $active);
    $view->with('menus', $query);
});

View::composer('partials.site.navbar', function($view) {
    $categories = ProjectCategories::lists("title", "id");
    $route = Request::segment(1);
    $view->with('active', $route);
    $view->with('categories', $categories);
});

View::composer(array('partials.site.footer', 'site.contact.index'), function($view) {
    $data = Home::find(1);

    $view->with('tel', (!empty($data)) ? str_replace(')', '</b>', str_replace('(', '<b>', $data->phone)) : "");
    $view->with('filial_tel', (!empty($data)) ? str_replace(')', '</b>', str_replace('(', '<b>', $data->filial_phone)) : "");
    $view->with('face', (!empty($data)) ? $data->facebook : "");
    $view->with('address', (!empty($data)) ? "$data->logradouro $data->number, $data->neighborhood - $data->city/$data->state" : "");
    $view->with('filial_address', (!empty($data)) ? "$data->filial_logradouro $data->filial_number, $data->filial_neighborhood - $data->filial_city/$data->filial_state" : "");
});
