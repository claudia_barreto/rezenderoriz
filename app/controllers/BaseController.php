<?php

class BaseController extends Controller {

  public function __construct() {
    
  }

  protected function setupLayout() {
    if (!is_null($this->layout)) {
      $this->layout = View::make($this->layout);
    }
  }

  public function __set($name, $value) {
    $this->variaveis[$name] = $value;
  }

  public function __get($name) {
    return isset($this->$name) ? $this->variaveis[$name] : null;
  }

  public function __isset($name) {
    return array_key_exists($name, $this->variaveis);
  }

  public function __unset($name) {
    if (isset($this->$name)) {
      unset($this->variaveis[$name]);
    }
  }

  protected function view($name) {
    $this->layout->content = \View::make($this->baseFolder . $name, $this->variaveis);
  }

  protected function json(array $v) {
    return \Response::json($v);
  }

  protected function html($content) {
    $response = Response::make($content);
    $response->header('Content-Type', 'UTF-8');
    return $response;
  }

}
