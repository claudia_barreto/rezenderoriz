<?php

namespace site;

class WhoWeAreController extends \SiteController {

    public function getIndex() {
        $this->page = \Input::get('p', false);
        $this->model = \AboutTheCompany::find(1);
        $this->companies = \GroupsCompanies::get();
        $this->clients = \Clients::get();
        return $this->view('who-we-are.index');
    }

}
