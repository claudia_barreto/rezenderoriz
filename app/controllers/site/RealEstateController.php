<?php

namespace site;

class RealEstateController extends \SiteController {

    public function getIndex() {
        $search = \Input::get('search', false);
        $category = \Input::get('c', false);
        if (!$category) {
            $this->title = "Todos";
        } else {
            $cat = \ProjectCategories::find($category);
            $this->title = $cat->title;
        }
        $this->model = \RealEstate::search($search, $category);
        return $this->view('real-estate.index');
    }

    public function getDetail($id) {
        $this->model = \RealEstate::with(array('images', 'plants', 'investments', 'category'))->find($id);
        return $this->view('real-estate.detail');
    }

}
