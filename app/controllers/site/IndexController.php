<?php

namespace site;

class IndexController extends \SiteController {

    public function getIndex() {
        $this->model = \Home::with(array("images"))->find(1);
        return $this->view('index.index');
    }

}
