<?php

namespace site;

class CustomerPortalController extends \SiteController {

    public function getIndex() {
        return $this->view('customer-portal.index');
    }

    public function postSend() {
        $all_input = \Input::all();

        $validator = \Validator::make(
                        $all_input, array(
                    'message_txt' => "required"
                    , 'name' => "required"
                    , 'email' => "required|email"
                    , 'ddd1' => "required|max:2"
                    , 'phone1' => "required|max:9"
                    , 'city' => "required"
                    , 'state' => "required|max:2"
                        )
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            $msg = "";
            foreach ($messages->all(':message<br/>') as $value) {
                $msg .= $value;
            }

            return $this->json(array(
                        'status' => false,
                        'msg' => $msg
            ));
        }
        
        $all_input['url'] = url('assets/img/site/logo-white.png');
        \Mail::send('emails.site.customer-portal', $all_input, function($message) use($all_input) {
            $message->from($all_input['email'], $all_input['name'])
                    ->to('suporte@emiolo.com', 'Suporte eMiolo.com')
                    ->subject('Portal do Cliente');
        });
        if (count(\Mail::failures()) == 0) {
            return $this->json(array(
                        'error' => false,
                        'msg' => "Enviado com sucesso."
            ));
        } else {
            return $this->json(array(
                        'error' => true,
                        'msg' => "Possível erro técnico, tente novamente."
            ));
        }
    }

}
