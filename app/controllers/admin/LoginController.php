<?php

namespace admin;

class LoginController extends \LoginController {

    public function getIndex() {
        return $this->view('login.index');
    }

    public function postIndex() {
        try {
            $username = \Input::get('username');
            $password = \Input::get('password');
            $remember = (\Input::has('remember')) ? true : false;

            $validator = \Validator::make(
                            array(
                        'username' => $username
                        , 'password' => $password
                            )
                            , array(
                        'username' => "required"
                        , 'password' => "required"
                            )
            );
            if ($validator->fails()) {
                $messages = $validator->messages();
                $msg = "";
                foreach ($messages->all(':message<br/>') as $value) {
                    $msg .= $value;
                }
                return $this->json(array(
                            'error' => true,
                            'msg' => $msg
                ));
            }

            if (\Auth::admin()->attempt(array('username' => $username, 'password' => $password, 'active' => 'Y'), $remember)) {
                return $this->json(array('error' => false));
            } else {
                return $this->json(array(
                            'error' => true,
                            'msg' => "Usuário e/ou Senha incorreto(s)."
                ));
            }
        } catch (\Exception $e) {
            return $this->json(array(
                        'error' => true,
                        'msg' => $e->getMessage()
            ));
        }
    }

    public function getLogout() {
        \Auth::admin()->logout();
        \Cache::forget('routeCache');
        \Cache::forget('menuCache');
        \Cache::forget('TitleCache');
        return \Redirect::to('admin/login');
    }

    public function getForgotPassword() {
        return $this->view('forgot.index');
    }

    public function postSendPassword() {
        try {
            $email = \Input::get('email');

            return $this->json(array(
                        'error' => true,
                        'msg' => "E-mail não existe."
            ));
        } catch (\Exception $e) {
            return $this->json(array(
                        'error' => true,
                        'msg' => "Possível erro técnico, tente novamente." //$e->getMessage()
            ));
        }
    }

}
