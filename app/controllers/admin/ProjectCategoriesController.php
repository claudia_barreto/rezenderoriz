<?php

namespace admin;

class ProjectCategoriesController extends \AdminController {

    public function getIndex() {
        return $this->view('project-categories.index');
    }

    public function getNew() {
        return $this->view('project-categories.new');
    }

    public function getEdit($id) {
        $this->model = \ProjectCategories::find($id);
        return $this->view('project-categories.edit');
    }

    public function postDelete($id) {
        $model = \ProjectCategories::find($id);

        if ($model->delete()) {
            return $this->json(array(
                        'error' => false,
                        'msg' => "Registro excluído com sucesso."
            ));
        } else {
            return $this->json(array(
                        'error' => true,
                        'msg' => "Não foi possível concluir a operação, tente novamente."
            ));
        }
    }

    public function postSave() {
        $id = \Input::get('id');
        $all_inputs = \Input::all();

        $validator = \Validator::make($all_inputs, \ProjectCategories::rules($id));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $msg = "";
            foreach ($messages->all(':message<br/>') as $value) {
                $msg .= $value;
            }
            return $this->json(array(
                        'error' => true,
                        'msg' => $msg
            ));
        }

        if (!empty($id)) {
            $model = \ProjectCategories::find($id);
        } else {
            $model = new \ProjectCategories();
        }

        $model->fill($all_inputs);

        if ($model->save()) {
            return $this->json(array(
                        'error' => false,
                        'msg' => "Cadastro concluido com sucesso."
            ));
        } else {
            return $this->json(array(
                        'error' => true,
                        'msg' => "Não foi possível concluir o cadastro, tente novamente."
            ));
        }
    }

    public function postListing() {
        $return = \ProjectCategories::listing();

        return $this->json(array(
                    'Result' => "OK",
                    'TotalRecordCount' => $return['count'],
                    'Records' => $return['rs']
        ));
    }

}
