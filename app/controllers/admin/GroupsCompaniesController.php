<?php

namespace admin;

class GroupsCompaniesController extends \AdminController {

    public function index() {
        $this->model = \GroupsCompanies::get();
        return $this->view('companies.index');
    }

    public function delete($id) {
        $model = \GroupsCompanies::find($id);
        $path = "assets/img/site/companies";
        $image = $model->image;

        if ($model->delete()) {
            if (!empty($image)) {
                if (\File::isFile("$path/$image")) {
                    \File::delete("$path/$image");
                }
            }

            return $this->json(array(
                        'error' => false,
                        'msg' => "Imagem excluída com sucesso."
            ));
        } else {
            return $this->json(array(
                        'error' => true,
                        'msg' => "Não foi possível concluir a operação, tente novamente."
            ));
        }
    }

    public function edit($id) {
        $model = \GroupsCompanies::find($id);

        $model->image = $model->image;
        $model->link = \Input::get('link');

        if ($model->save()) {
            return $this->json(array(
                        'error' => false,
                        'msg' => "Cadastro alterado com sucesso."
            ));
        } else {
            return $this->json(array(
                        'error' => true,
                        'msg' => "Não foi possível concluir a alteração do cadastro, tente novamente."
            ));
        }
    }

    public function save() {
        $image = \Input::file('image');
        $all_inputs = \Input::all();

        $validator = \Validator::make($all_inputs, \GroupsCompanies::rules());
        if ($validator->fails()) {
            $messages = $validator->messages();
            $msg = "";
            foreach ($messages->all(':message<br/>') as $value) {
                $msg .= $value;
            }
            return $this->json(array(
                        'error' => true,
                        'msg' => $msg
            ));
        }

        $model = new \GroupsCompanies();

        $model->fill($all_inputs);

        if (!empty($image)) {
            $path = "assets/img/site/companies";
            if (!\File::isDirectory($path)) {
                \File::makeDirectory($path, 777, true);
            }

            $img = md5(date("dmYHis") . $image) . "." . $image->getClientOriginalExtension();
            $intervation = \Image::make($image);
            $intervation->resize(130, 130, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $intervation->save("$path/$img");

            $model->image = $img;
        }

        if ($model->save()) {
            return $this->json(array(
                        'error' => false,
                        'msg' => "Cadastro concluido com sucesso."
            ));
        } else {
            return $this->json(array(
                        'error' => true,
                        'msg' => "Não foi possível concluir o cadastro, tente novamente."
            ));
        }
    }

}
