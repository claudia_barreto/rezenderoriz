<?php

namespace admin;

class RealEstateController extends \AdminController {

    public function getIndex() {
        return $this->view('real-estate.index');
    }

    public function getNew() {
        $this->categories = \ProjectCategories::lists("title", "id");
        return $this->view('real-estate.new');
    }

    public function getEdit($id) {
        $this->categories = \ProjectCategories::lists("title", "id");
        $this->model = \RealEstate::with(array('images', 'plants', 'investments'))->find($id);
        return $this->view('real-estate.edit');
    }

    public function postDelete($id) {
        $model = \RealEstate::find($id);

        if ($model->delete()) {
            return $this->json(array(
                        'error' => false,
                        'msg' => "Registro excluído com sucesso."
            ));
        } else {
            return $this->json(array(
                        'error' => true,
                        'msg' => "Não foi possível concluir a operação, tente novamente."
            ));
        }
    }

    public function postDeleteInvestment($id) {
        $model = \RealEstateInvestment::find($id);

        if ($model->delete()) {
            return $this->json(array(
                        'error' => false,
                        'msg' => "Registro excluído com sucesso."
            ));
        } else {
            return $this->json(array(
                        'error' => true,
                        'msg' => "Não foi possível concluir a operação, tente novamente."
            ));
        }
    }

    public function postSaveUpdates($id) {
        $description = \Input::get("description");
        $model = \RealEstateImagesPlants::find($id);

        $model->description = $description;

        if ($model->save()) {
            return $this->json(array(
                        'error' => false,
                        'msg' => "Alteração efetuada com sucesso."
            ));
        } else {
            return $this->json(array(
                        'error' => true,
                        'msg' => "Não foi possível efetuar a alteração, tente novamente."
            ));
        }
    }

    public function postDeleteImage($id) {
        $model = \RealEstateImagesPlants::find($id);
        $path = "assets/img/site/real-estate";
        $image = $model->image;

        if ($model->delete()) {
            if (!empty($image)) {
                if (\File::isFile("$path/$image")) {
                    \File::delete("$path/$image");
                }
            }

            return $this->json(array(
                        'error' => false,
                        'msg' => "Imagem excluída com sucesso."
            ));
        } else {
            return $this->json(array(
                        'error' => true,
                        'msg' => "Não foi possível concluir a operação, tente novamente."
            ));
        }
    }

    public function postSave() {
        $id = \Input::get('id');
        $description_i = \Input::get('description_i');
        $images = \Input::file('images');
        $plants = \Input::file('plants');
        $all_inputs = \Input::all();

        $validator = \Validator::make($all_inputs, \RealEstate::rules($id));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $msg = "";
            foreach ($messages->all(':message<br/>') as $value) {
                $msg .= $value;
            }
            return $this->json(array(
                        'error' => true,
                        'msg' => $msg
            ));
        }

        if (!empty($id)) {
            $model = \RealEstate::find($id);
        } else {
            $model = new \RealEstate();
        }

        $model->fill($all_inputs);

        if ($model->save()) {
            $id = $model->id;

            $path = "assets/img/site/real-estate";
            if (!\File::isDirectory($path)) {
                \File::makeDirectory($path, 777);
            }

            if (!empty($images)) {
                foreach ($images as $value) {
                    $image = "";
                    if (!empty($value)) {
                        $image = md5(date("dmYHis") . $value) . "." . $value->getClientOriginalExtension();
                        \Image::make($value)->save("$path/$image");
                    }

                    $inputs = [
                        'type' => 'G',
                        'image' => $image,
                        'real_estate_id' => $id
                    ];

                    $model = new \RealEstateImagesPlants();

                    $model->fill($inputs);

                    $model->save();
                }
            }

            if (!empty($plants)) {
                $image = md5(date("dmYHis") . $plants) . "." . $plants->getClientOriginalExtension();
                \Image::make($plants)->save("$path/$image");

                $inputs = [
                    'type' => 'P',
                    'image' => $image,
                    'description' => $all_inputs['description_plants'],
                    'real_estate_id' => $id
                ];

                $model = new \RealEstateImagesPlants();
                $model->fill($inputs);
                $model->save();
            }

            if (!empty($description_i)) {
                foreach ($description_i as $key => $value) {
                    $real_estate_investment_id = isset($all_inputs['real_estate_investment_id'][$key]) ? $all_inputs['real_estate_investment_id'][$key] : '';

                    if (!empty($real_estate_investment_id)) {
                        $model = \RealEstateInvestment::find($real_estate_investment_id);
                    } else {
                        $model = new \RealEstateInvestment();
                    }

                    $inputs = [
                        'description' => $value,
                        'real_estate_id' => $id
                    ];

                    $model->fill($inputs);

                    $model->save();
                }
            }

            return $this->json(array(
                        'error' => false,
                        'msg' => "Cadastro concluido com sucesso."
            ));
        } else {
            return $this->json(array(
                        'error' => true,
                        'msg' => "Não foi possível concluir o cadastro, tente novamente."
            ));
        }
    }

    public function postListing() {
        $return = \RealEstate::listing();

        return $this->json(array(
                    'Result' => "OK",
                    'TotalRecordCount' => $return['count'],
                    'Records' => $return['rs']
        ));
    }

}
