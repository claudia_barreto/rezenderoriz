<?php

namespace admin;

class IndexController extends \AdminController {

    public function getIndex() {
        return $this->view('index.index');
    }

}
