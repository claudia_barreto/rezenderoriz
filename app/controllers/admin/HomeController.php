<?php

namespace admin;

class HomeController extends \AdminController {

    public function getIndex() {
        $this->model = \Home::with('images')->find(1);
        return $this->view('home.index');
    }

    public function postDeleteBanner($id) {
        $model = \HomeImages::find($id);
        $path = "assets/img/site/home";
        $image = $model->image;

        if ($model->delete()) {
            if (!empty($image)) {
                if (\File::isFile("$path/$image")) {
                    \File::delete("$path/$image");
                }
            }

            return $this->json(array(
                        'error' => false,
                        'msg' => "Imagem excluída com sucesso."
            ));
        } else {
            return $this->json(array(
                        'error' => true,
                        'msg' => "Não foi possível concluir a operação, tente novamente."
            ));
        }
    }

    public function postSave() {
        $id = \Input::get('id');
        $images = \Input::file('images');
        $image_left = \Input::file('image_left');
        $image_right = \Input::file('image_right');
        $all_inputs = \Input::all();

        $validator = \Validator::make($all_inputs, \Home::rules($id));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $msg = "";
            foreach ($messages->all(':message<br/>') as $value) {
                $msg .= $value;
            }
            return $this->json(array(
                        'error' => true,
                        'msg' => $msg
            ));
        }

        $n_image_left = $n_image_right = "";
        if (!empty($id)) {
            $model = \Home::find($id);
            $n_image_left = $model->image_left;
            $n_image_right = $model->image_right;
        } else {
            $model = new \Home();
        }

        $model->fill($all_inputs);

        $path = "assets/img/site/home";
        if (!empty($image_left)) {
            if (!\File::isDirectory($path)) {
                \File::makeDirectory($path, 777);
            }

            $n_image_left = "quem-somos." . $image_left->getClientOriginalExtension();
            \Image::make($image_left)->save("$path/$n_image_left");
        }

        if (!empty($image_right)) {
            if (!\File::isDirectory($path)) {
                \File::makeDirectory($path, 777);
            }

            $n_image_right = "duvidas." . $image_right->getClientOriginalExtension();
            \Image::make($image_right)->save("$path/$n_image_right");
        }

        $model->image_left = $n_image_left;
        $model->image_right = $n_image_right;


        if ($model->save()) {
            $id = $model->id;

            foreach ($images as $key => $value) {
                $home_images_id = isset($all_inputs['home_images_id'][$key]) ? $all_inputs['home_images_id'][$key] : '';

                $image = "";
                if (!empty($home_images_id)) {
                    $model = \HomeImages::find($home_images_id);
                    $image = $model->image;
                } else {
                    $model = new \HomeImages();
                }

                if (!empty($value)) {
                    if (!\File::isDirectory($path)) {
                        \File::makeDirectory($path, 777);
                    }

                    $image = md5(date("dmYHis") . $value) . "." . $value->getClientOriginalExtension();
                    \Image::make($value)->save("$path/$image");
                }

                $inputs = [
                    'image' => $image,
                    'home_id' => $id
                ];

                $model->fill($inputs);

                $model->save();
            }

            return $this->json(array(
                        'error' => false,
                        'msg' => "Cadastro concluido com sucesso."
            ));
        } else {
            return $this->json(array(
                        'error' => true,
                        'msg' => "Não foi possível concluir o cadastro, tente novamente."
            ));
        }
    }

}
