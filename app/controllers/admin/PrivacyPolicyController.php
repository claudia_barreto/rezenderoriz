<?php

namespace admin;

class PrivacyPolicyController extends \AdminController {

    public function getIndex() {
        $this->model = \AboutTheCompany::find(1);
        return $this->view('privacy-policy.index');
    }

    public function postSave() {
        $id = \Input::get('id');
        $files = \Input::file('file');
        $files->getClientOriginalExtension();
        $all_inputs = \Input::all();

        $validator = \Validator::make($all_inputs, ['file' => 'required|mimes:pdf,PDF']);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $msg = "";
            foreach ($messages->all(':message<br/>') as $value) {
                $msg .= $value;
            }
            return $this->json(array(
                        'error' => true,
                        'msg' => $msg
            ));
        }

        $path = "assets";
        if (!\File::isDirectory($path)) {
            \File::makeDirectory($path, 777);
        }

        $file = "politica-privacidade." . $files->getClientOriginalExtension();
        $save = \File::move($files, "$path/$file");

        if ($save) {
            return $this->json(array(
                        'error' => false,
                        'msg' => "Cadastro concluido com sucesso."
            ));
        } else {
            return $this->json(array(
                        'error' => true,
                        'msg' => "Não foi possível concluir o cadastro, tente novamente."
            ));
        }
    }

}
