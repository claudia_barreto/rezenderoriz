<?php

namespace admin;

class UserController extends \AdminController {

    public function getIndex() {
        return $this->view('user.index');
    }

    public function getNew() {
        $this->groups = \Group::lists('name', 'id');
        return $this->view('user.new');
    }

    public function getEdit($id) {
        $this->groups = \Group::lists('name', 'id');
        $this->model = \User::find($id);
        return $this->view('user.edit');
    }

    public function postDelete($id) {
        $model = \User::find($id);
        $path = "assets/img/admin/user";
        $avatar = $model->avatar;

        if ($model->delete()) {
            /*if (!empty($avatar)) {
                if (\File::isFile("$path/$avatar")) {
                    \File::delete("$path/$avatar");
                }
            }*/

            return $this->json(array(
                        'error' => false,
                        'msg' => "Registro excluído com sucesso."
            ));
        } else {
            return $this->json(array(
                        'error' => true,
                        'msg' => "Não foi possível concluir a operação, tente novamente."
            ));
        }
    }

    public function postSave() {
        $id = \Input::get('id');
        $password = \Input::get('password');
        $avatar = \Input::file('avatar');
        $all_inputs = \Input::all();

        $validator = \Validator::make($all_inputs, \User::rules($id, $password));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $msg = "";
            foreach ($messages->all(':message<br/>') as $value) {
                $msg .= $value;
            }
            return $this->json(array(
                        'error' => true,
                        'msg' => $msg
            ));
        }

        $file = "";
        if ($id) {
            $model = \User::find($id);
            $file = $model->avatar;
        } else {
            $model = new \User();
        }

        $model->fill($all_inputs);

        if (!empty($avatar)) {
            $path = "assets/img/admin/user";
            if (!\File::isDirectory($path)) {
                \File::makeDirectory($path, 777);
            }

            $file = md5($all_inputs['username']) . "." . $avatar->getClientOriginalExtension();
            \Image::make($avatar)->resize(60, 60)->save("$path/$file");
        }

        $model->avatar = $file;
        $model->password = ($password) ? \Hash::make($password) : $model->password;

        if ($model->save()) {
            return $this->json(array(
                        'error' => false,
                        'msg' => "Cadastro concluido com sucesso."
            ));
        } else {
            return $this->json(array(
                        'error' => true,
                        'msg' => "Não foi possível concluir o cadastro, tente novamente."
            ));
        }
    }

    public function postListing() {
        $return = \User::listing();

        return $this->json(array(
                    'Result' => "OK",
                    'TotalRecordCount' => $return['count'],
                    'Records' => $return['rs']
        ));
    }

}
