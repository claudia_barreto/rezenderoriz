<?php

namespace admin;

class AboutTheCompanyController extends \AdminController {

    public function getIndex() {
        $this->model = \AboutTheCompany::find(1);
        return $this->view('about-the-company.index');
    }

    public function postSave() {
        $id = \Input::get('id');
        $image = \Input::file('image');
        $all_inputs = \Input::all();

        $validator = \Validator::make($all_inputs, \AboutTheCompany::rules($id));
        if ($validator->fails()) {
            $messages = $validator->messages();
            $msg = "";
            foreach ($messages->all(':message<br/>') as $value) {
                $msg .= $value;
            }
            return $this->json(array(
                        'error' => true,
                        'msg' => $msg
            ));
        }

        $file = "";
        if ($id) {
            $model = \AboutTheCompany::find($id);
            $file = $model->image;
        } else {
            $model = new \AboutTheCompany();
        }

        $model->fill($all_inputs);

        if (!empty($image)) {
            $path = "assets/img/site";
            if (!\File::isDirectory($path)) {
                \File::makeDirectory($path, 777);
            }

            $file = "politica." . $image->getClientOriginalExtension();
            \Image::make($image)->save("$path/$file");
        }

        $model->image = $file;

        if ($model->save()) {
            return $this->json(array(
                        'error' => false,
                        'msg' => "Cadastro concluido com sucesso."
            ));
        } else {
            return $this->json(array(
                        'error' => true,
                        'msg' => "Não foi possível concluir o cadastro, tente novamente."
            ));
        }
    }

}
