<?php

class RealEstate extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'real_estate';
    protected $fillable = array('name', 'description', 'neighborhood', 'city', 'room', 'project_categories_id');
    protected $dates = ['created_at', 'updated_at'];
    static $rules = [
        'name' => "required",
        'description' => "required",
        'neighborhood' => "required",
        'city' => "required",
        'neighborhood' => "required",
        'room' => "required",
        'project_categories_id' => "required",
    ];

    public static function rules($id) {
        $rules = static::$rules;

        return $rules;
    }

    public function images() {
        return $this->hasMany('RealEstateImagesPlants', 'real_estate_id')->where('type', 'G');
    }

    public function plants() {
        return $this->hasMany('RealEstateImagesPlants', 'real_estate_id')->where('type', 'P');
    }

    public function investments() {
        return $this->hasMany('RealEstateInvestment', 'real_estate_id');
    }

    public function category() {
        return $this->belongsTo("ProjectCategories", "project_categories_id");
    }

    public static function listing() {
        $pagina = \Input::get('jtStartIndex');
        $limite = \Input::get('jtPageSize');
        $orderby = explode(" ", \Input::get('jtSorting'));
        $orderby = array('field' => $orderby[0], 'order' => $orderby[1]);

        $rs = self::with('category')->where(function($query) {
                    $search = \Input::get('search');
                    if (!empty($search)) {
                        $query->where('name', 'LIKE', "%$name%")
                        ->orWhere('description', 'LIKE', "%$name%")
                        ->orWhere('neighborhood', 'LIKE', "%$name%")
                        ->orWhere('city', 'LIKE', "%$name%")
                        ->orWhere('room', 'LIKE', "%$name%");
                    }
                })
                ->orderBy($orderby['field'], $orderby['order']);
        $count = $rs->count();
        $rs = $rs->skip($pagina)
                ->take($limite)
                ->get()
                ->toArray();

        return ['count' => $count, 'rs' => $rs];
    }

    public static function search($search = false, $category = false) {
        return self::with('images')
                        ->where(function($q) use($search) {
                            if ($search) {
                                $q->orWhere('name', 'LIKE', "%$search%")
                                ->orWhere('description', 'LIKE', "%$search%")
                                ->orWhere('neighborhood', 'LIKE', "%$search%")
                                ->orWhere('city', 'LIKE', "%$search%")
                                ->orWhere('room', 'LIKE', "%$search%");
                            }
                        })
                        ->where(function($q) use($category) {
                            if ($category) {
                                $q->where('project_categories_id', $category);
                            }
                        })
                        ->paginate(8);
    }

}
