<?php

class ProjectCategories extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'project_categories';
    protected $fillable = array('title');
    protected $dates = ['created_at', 'updated_at'];
    static $rules = [
        'title' => "required|unique:project_categories,title"
    ];

    public static function rules($id) {
        $rules = static::$rules;

        if ($id) {
            $rules['title'] = $rules['title'] . "," . $id;
        }

        return $rules;
    }

    public static function listing() {
        $pagina = \Input::get('jtStartIndex');
        $limite = \Input::get('jtPageSize');
        $orderby = explode(" ", \Input::get('jtSorting'));
        $orderby = array('field' => $orderby[0], 'order' => $orderby[1]);

        $rs = self::where(function($query) {
                    $search = \Input::get('search');
                    if (!empty($search)) {
                        $query->where('title', 'LIKE', "%$search%");
                    }
                })
                ->orderBy($orderby['field'], $orderby['order']);
        $count = $rs->count();
        $rs = $rs->skip($pagina)
                ->take($limite)
                ->get()
                ->toArray();

        return ['count' => $count, 'rs' => $rs];
    }

}
