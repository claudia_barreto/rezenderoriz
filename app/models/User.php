<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class User extends BaseModel implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait,
        SoftDeletingTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';
    protected $fillable = array('name', 'email', 'username', 'password', 'avatar', 'active', 'group_id');
    static $rules = [
        'name' => "required",
        'group_id' => "required",
        'password' => "required|between:6,15",
        'password_confirm' => "required|same:password",
        'username' => "required|unique:user,username"
    ];
    protected $appends = ['photo'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public static function boot() {
        if (\Auth::admin()->check()) {
            parent::boot();
            $id = \Auth::admin()->user()->id;
            static::creating(function($model) use($id) {
                $model->created_by = $id;
                $model->updated_by = $id;
            });
            static::updating(function($model) use($id) {
                $model->updated_by = $id;
            });
            static::deleting(function($model) use($id) {
                $model->deleted_by = $id;
            });
        }
    }

    public function getPhotoAttribute() {
        return url("assets/img/admin/user/" . (!empty($this->attributes['avatar']) ? $this->attributes['avatar'] : "../profile.png"));
    }

    public function group() {
        return $this->belongsTo('Group', 'group_id');
    }

    public static function rules($id, $password) {
        $rules = static::$rules;
        if ($id) {
            $rules['username'] = $rules['username'] . ',' . $id;

            if (!$password) {
                unset($rules['password'], $rules['password_confirm']);
            }
        }

        return $rules;
    }

    public static function listing() {
        $pagina = \Input::get('jtStartIndex');
        $limite = \Input::get('jtPageSize');
        $orderby = explode(" ", \Input::get('jtSorting'));
        $orderby = array('field' => $orderby[0], 'order' => $orderby[1]);

        $rs = self::with('group')
                ->where(function($query) {
                    $name = \Input::get('name');
                    if (!empty($name)) {
                        $query->where('name', 'LIKE', "%$name%");
                    }
                })
                ->orderBy($orderby['field'], $orderby['order']);
        $count = $rs->count();
        $rs = $rs->skip($pagina)
                ->take($limite)
                ->get()
                ->toArray();

        return ['count' => $count, 'rs' => $rs];
    }

}
