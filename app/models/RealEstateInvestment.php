<?php

class RealEstateInvestment extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'real_estate_investment';
    protected $fillable = array('description', 'real_estate_id');
    protected $dates = ['created_at', 'updated_at'];
    static $rules = [
        'description' => "required"
    ];

    public static function rules($id) {
        $rules = static::$rules;

        return $rules;
    }

    public function realEstate() {
        return $this->belongsTo('RealEstate', 'real_estate_id');
    }

}
