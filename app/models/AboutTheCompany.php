<?php

class AboutTheCompany extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'about_the_company';
    protected $fillable = array('title', 'text', 'text_missao', 'text_visao', 'text_valores', 'image');
    protected $dates = ['created_at', 'updated_at'];
    static $rules = [
        'title' => "required",
        'text' => "required",
        'text_missao' => "required",
        'text_visao' => "required",
        'text_valores' => "required"
    ];
    protected $appends = ['img'];

    public static function rules($id) {
        $rules = static::$rules;

        return $rules;
    }

    public function getImgAttribute() {
        return url("assets/img/site/" . (!empty($this->attributes['image']) ? $this->attributes['image'] : ""));
    }

}
