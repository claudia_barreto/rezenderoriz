<?php

class RealEstateImagesPlants extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'real_estate_images_plants';
    protected $fillable = array('type', 'image', 'description', 'real_estate_id');
    protected $dates = ['created_at', 'updated_at'];
    static $rules = [
        'type' => "required",
        'image' => "required"
    ];
    protected $appends = ['img'];

    public static function rules($id) {
        $rules = static::$rules;

        return $rules;
    }

    public function getImgAttribute() {
        return url("assets/img/site/real-estate/" . (!empty($this->attributes['image']) ? $this->attributes['image'] : "http://placehold.it/199x140"));
    }

    public function realEstate() {
        return $this->belongsTo('RealEstate', 'real_estate_id');
    }

}
