<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class SubMenu extends BaseModel {

    use SoftDeletingTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'submenu';
    protected $fillable = array('name', 'route', 'active', 'menu_id');
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public static function boot() {
        if (\Auth::admin()->check()) {
            parent::boot();
            $id = \Auth::admin()->user()->id;
            static::creating(function($model) use($id) {
                $model->created_by = $id;
                $model->updated_by = $id;
            });
            static::updating(function($model) use($id) {
                $model->updated_by = $id;
            });
            static::deleting(function($model) use($id) {
                $model->deleted_by = $id;
            });
        }
    }

    public function menus() {
        return $this->belongsTo('Menu', 'menu_id');
    }

    public function groups() {
        return $this->belongsToMany('Group', 'submenu_has_group', 'submenu_id', 'group_id');
    }

    public static function permissionAccessRoute($route) {
        return \Cache::remember('routeCache' . $route, 60, function() use($route) {
                    return self::with(array('groups' => function($query) use($route) {
                                    $query->where('submenu_has_group.group_id', '=', \Auth::admin()->user()->group_id);
                                }))->where('submenu.route', '=', $route)->active()->take(1)->get();
                });
    }

    public function scopeActive($q) {
        return $q->where("active", "=", "Y");
    }

    public function scopePermission($q, $group_id) {
        return $q->whereHas('groups', function($query) use($group_id) {
                    $query->where('submenu_has_group.group_id', '=', $group_id);
                });
    }

}
