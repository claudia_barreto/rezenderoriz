<?php

class Home extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'home';
    protected $fillable = array('image_left', 'link_left', 'image_right', 'link_right', 'phone', 'facebook', 'logradouro', 'number', 'complement', 'neighborhood', 'city', 'state', 'filial_phone', 'filial_logradouro', 'filial_number', 'filial_complement', 'filial_neighborhood', 'filial_city', 'filial_state');
    protected $dates = ['created_at', 'updated_at'];
    static $rules = [
        //'link_left' => "required|max:250",
        //'link_right' => "required|max:250",
        'phone' => "required|max:14",
        'facebook' => "required|max:250",
        'logradouro' => "required|max:250",
        'number' => "required|max:10",
        'neighborhood' => "required|max:150",
        'city' => "required|max:150",
        'state' => "required|max:2",
    ];
    protected $appends = ['img_left', 'img_right'];

    public static function rules($id) {
        $rules = static::$rules;

        return $rules;
    }

    public function getImgLeftAttribute() {
        return (!empty($this->attributes['image_left']) ? url("assets/img/site/home/" . $this->attributes['image_left']) : "http://placehold.it/479x231");
    }

    public function getImgRightAttribute() {
        return (!empty($this->attributes['image_right']) ? url("assets/img/site/home/" . $this->attributes['image_right']) : "http://placehold.it/479x231");
    }

    public function images() {
        return $this->hasMany('HomeImages', 'home_id');
    }

}
