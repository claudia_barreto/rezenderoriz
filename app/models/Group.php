<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Group extends BaseModel {

    use SoftDeletingTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'group';
    protected $fillable = array('name', 'active');
    static $rules = [
        'name' => "required|unique:group,name",
        'active' => "required"
    ];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public static function boot() {
        if (\Auth::admin()->check()) {
            parent::boot();
            $id = \Auth::admin()->user()->id;
            static::creating(function($model) use($id) {
                $model->created_by = $id;
                $model->updated_by = $id;
            });
            static::updating(function($model) use($id) {
                $model->updated_by = $id;
            });
            static::deleting(function($model) use($id) {
                $model->deleted_by = $id;
            });
        }
    }

    public static function selectPermission() {
        $user = \Auth::admin()->user();
        return self::where(function($w) use($user) {
                    if (!in_array($user->group_id, array(1, 2))) {
                        $w->where("id", "=", $user->group_id);
                    }
                })->lists('name', 'id');
    }

    public function user() {
        return $this->belongsTo('User', 'group_id');
    }

    public function subMenus() {
        return $this->belongsToMany('SubMenu', 'submenu_has_group', 'group_id', 'submenu_id');
    }

    public function scopeActive($q) {
        return $q->where("active", "=", "Y");
    }

    public static function rules($id) {
        $rules = static::$rules;
        if ($id) {
            $rules['name'] = $rules['name'] . ',' . $id;
        }

        return $rules;
    }

}
