<?php

class Clients extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clients';
    protected $fillable = array('image', 'link');
    protected $dates = ['created_at', 'updated_at'];
    static $rules = [
        'image' => "required",
        'link' => "required|max:250"
    ];
    protected $appends = ['img'];

    public static function rules() {
        $rules = static::$rules;

        return $rules;
    }

    public function getImgAttribute() {
        return (!empty($this->attributes['image']) ? url("assets/img/site/clients/" . $this->attributes['image']) : "http://placehold.it/130x130");
    }

}
