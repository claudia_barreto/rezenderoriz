<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Menu extends BaseModel {

    use SoftDeletingTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menu';
    protected $fillable = array('name', 'icon', 'route', 'active');
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public static function boot() {
        if (\Auth::admin()->check()) {
            parent::boot();
            $id = \Auth::admin()->user()->id;
            static::creating(function($model) use($id) {
                $model->created_by = $id;
                $model->updated_by = $id;
            });
            static::updating(function($model) use($id) {
                $model->updated_by = $id;
            });
            static::deleting(function($model) use($id) {
                $model->deleted_by = $id;
            });
        }
    }

    public function subMenus() {
        return $this->hasMany('SubMenu', 'menu_id');
    }

    public static function title($route) {
        return \Cache::remember('TitleCache' . $route, 60, function() use($route) {
                    return self::with(array('subMenus' => function($query) use($route) {
                                            $query->where('route', '=', $route);
                                        }))
                                    ->whereHas('subMenus', function($query) use($route) {
                                        $query->where('route', '=', $route);
                                    })
                                    ->take(1)->get()->toArray();
                });
    }

    public static function menus() {
        $group_id = \Auth::admin()->user()->group_id;
        return \Cache::remember('menuCache' . $group_id, null, function() use($group_id) {
                    return self::with(array('subMenus' => function($query) use($group_id) {
                                            $query->with(array('groups' => function($query) use($group_id) {
                                                    $query->where('group.id', '=', $group_id);
                                                    $query->active();
                                                }))
                                            ->whereHas('groups', function($query) use($group_id) {
                                                $query->where('group.id', '=', $group_id);
                                                $query->active();
                                            })->active();
                                        }))
                                            ->whereHas('subMenus', function($query) use($group_id) {
                                                $query->with(array('groups' => function($query) use($group_id) {
                                                        $query->where('group.id', '=', $group_id);
                                                        $query->active();
                                                    }))
                                                ->whereHas('groups', function($query) use($group_id) {
                                                    $query->where('group.id', '=', $group_id);
                                                    $query->active();
                                                })->active();
                                            })
                                            ->active()
                                            ->get()->toArray();
                        });
            }

            public function scopeActive($q) {
                return $q->where("active", "=", 'Y');
            }

        }
        