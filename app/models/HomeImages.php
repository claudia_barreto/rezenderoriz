<?php

class HomeImages extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'home_images';
    protected $fillable = array('image', 'home_id');
    protected $dates = ['created_at', 'updated_at'];
    static $rules = [
        'image' => "required"
    ];
    protected $appends = ['img'];

    public static function rules($id) {
        $rules = static::$rules;

        return $rules;
    }

    public function getImgAttribute() {
        return url("assets/img/site/home/" . (!empty($this->attributes['image']) ? $this->attributes['image'] : ""));
    }

    public function home() {
        return $this->hasMany('Home', 'home_id');
    }

}
