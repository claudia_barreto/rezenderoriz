<?php

return array(
    'multi' => array(
        'admin' => array(
            'driver' => 'eloquent',
            'model' => 'User'
        ),
        'site' => array(
            'driver' => 'eloquent',
            'table' => 'User'
        )
    ),
    'reminder' => array(
        'email' => 'emails.auth.reminder',
        'table' => 'password_reminders',
        'expire' => 60,
    ),
);
