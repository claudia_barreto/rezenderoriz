<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">        
        <meta name="description" content="Cerebelo">
        <meta name="author" content="eMiolo.com">

        <title>Rezende Roriz - Login Cerebelo</title>

        <link rel="shortcut icon" href="{{ url('assets/img/admin/favicon.png') }}" type="image/x-icon">

        <link href="{{ url('assets/css/admin/style.default.css') }}" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="signin">
        <section>
            @yield('content')
        </section>

        <script src="{{ url('assets/js/admin/jquery-1.11.1.min.js') }}"></script>
        <script src="{{ url('assets/js/admin/jquery-migrate-1.2.1.min.js') }}"></script>
        <script src="{{ url('assets/js/admin/bootstrap.min.js') }}"></script>
        <script src="{{ url('assets/js/admin/modernizr.min.js') }}"></script>
        <script src="{{ url('assets/js/admin/pace.min.js') }}"></script>
        <script src="{{ url('assets/js/admin/retina.min.js') }}"></script>
        <script src="{{ url('assets/js/admin/jquery.cookies.js') }}"></script>
        <script src="{{ url('assets/js/jquery.blockUI.js') }}"></script>
        <script src="{{ url('assets/js/jquery.form.min.js') }}"></script>

        <script src="{{ url('assets/js/admin/custom.js') }}"></script>
        <script src="{{ url('assets/js/jquery.noty.packaged.min.js') }}"></script>
        @yield('js')
    </body>
</html>
