<!DOCTYPE html>
<html class="ls-theme-gray">
    <head>
        <meta charset="utf-8">
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <meta name="description" content="Insira aqui a descrição da página.">
        <title>Rezende Roriz - Cerebelo</title>

        <link rel="shortcut icon" href="{{ url('assets/img/admin/favicon.png') }}" type="image/x-icon">

        @include('partials.admin.css')
        @yield('css')
    </head>

    <body>
        @include('partials.admin.header')
        <section>
            <div class="mainwrapper">
                @include('partials.admin.leftpanel')
                <div class="mainpanel">
                    @include('partials.admin.pageheader')
                    <div class="contentpanel">                        
                        @yield('content')
                    </div>
                </div>
            </div>
        </section>


        @include('partials.admin.js')
        @yield('js')
    </body>
</html>
