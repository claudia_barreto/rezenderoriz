<!DOCTYPE html>
<html class="ls-theme-gray">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Rezende Roriz | Incorporação e Construção</title>

        <!-- Bootstrap core CSS -->
        <link href="{{ url('assets/css/site/bootstrap.css') }}" rel="stylesheet">
        <link href="{{ url('assets/css/site/estilo.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ url('assets/css/site/loader-rezende.css') }}" rel="stylesheet" />

        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="{{ url('assets/img/site/favicon.png') }}"  />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
              <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
        @yield('css')
    </head>

    <body>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.3";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

        @include('partials.site.navbar')

        <!-- Begin page content -->
        <div class="container company-content">            
            @yield('content')

            <div class="clearfix"></div>
        </div>

        @include('partials.site.footer')

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script>window.url = "{{ url('/') }}"</script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{ url('assets/js/site/bootstrap.min.js') }}"></script>

        <script src="{{ url('assets/js/jquery.form.min.js') }}"></script>
        <script src="{{ url('assets/js/jquery.noty.packaged.min.js') }}"></script>
        .<script src="{{ url('assets/js/site/main.js') }}"></script>
        @yield('js')
    </body>
</html>
