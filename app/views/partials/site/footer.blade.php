<div id="footer">
    <div class="container">
        <p class="footer-block">
            <a href="{{ url('/') }}" title="Rezende Roriz"><img src="{{ url('assets/img/site/logo-white.png') }}" alt="Rezende Roriz"/></a>
        </p>	
        <div class="info">
            <span class="tel">{{ $tel }}</span>
            <span class="face"> <img src="{{ url('assets/img/site/face.png') }}" alt="Facebook Rezende Roriz"/> <a href="{{ $face }}" target="_blank" title="Facebook Rezende Roriz">facebook.com/RezendeRoriz</a></span>
            <span class="address">Matriz: {{ $address }}</span>
            <span class="address">Filial: {{ $filial_address }} - {{ $filial_tel }}</span>
        </div>
    </div>
</div>