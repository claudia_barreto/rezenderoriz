<div class="navbar navbar-default navbar-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}" title="Rezende Roriz"><img src="{{ url('assets/img/site/logo.png') }}" alt="Rezende Roriz"/></a>
        </div>
        <div class="collapse navbar-collapse">         
            <ul class="nav navbar-nav navbar-right">
                <li class="{{ ($active == "") ? "active" : "" }}"><a href="{{ url('/') }}" title="Home">Home</a></li>
                <li class="{{ (in_array($active, array("quem-somos", "politica-privacidade"))) ? "active" : "" }}"><a href="{{ url('quem-somos') }}" title="Quem Somos">Quem Somos</a></li>
                <li class="dropdown {{ (in_array($active, array("projetos"))) ? "active" : "" }}">                    
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle" title="Projetos" onclick="window.location.href='{{ url('projetos') }}'">Projetos <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('projetos') }}">Todos</a></li>
                        @foreach($categories as $id => $title)
                        <li><a href="{{ url('projetos?c=' . $id) }}">{{ $title }}</a></li>
                        @endforeach
                    </ul>
                </li>
                <li class="{{ ($active == "portal-do-cliente") ? "active" : "" }}"><a href="{{ url('portal-do-cliente') }}" title="Portal do Cliente">Portal do Cliente</a></li>  
                <li class="{{ ($active == "contato") ? "active" : "" }}"><a href="{{ url('contato') }}" title="Contato">Contato</a></li>
                
            </ul>
        </div><!--/.nav-collapse -->
        {{ Form::open(array('url' => 'imoveis', 'method' => 'get', 'id' => 'form')) }}
        <div class="search col-md-8 col-md-offset-4">
            <span>Busca Rápida</span>
            <button type="submit" class="btn btn-default search-button" aria-label="Left Align">
                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
            </button>
            {{ Form::text('search', null, ['class' => 'search-input']) }}

            <div class="clearfix"></div>
        </div>
        {{ Form::close() }}
    </div>
</div>