<div class="leftpanel">
    <div class="media profile-left">
        <a class="pull-left profile-thumb" href="{{ url('admin/usuarios/editar/' . \Auth::admin()->user()->id) }}">
            <img class="img-circle" src="{{ \Auth::admin()->user()->photo }}" alt="">
        </a>
        <div class="media-body">
            <h4 class="media-heading">{{ \Auth::admin()->user()->name }}</h4>
            <small class="text-muted">{{ \Auth::admin()->user()->group->name }}</small>
        </div>
    </div><!-- media -->

    <ul class="nav nav-pills nav-stacked">
        <li class="{{ (empty($menu_active) ? 'active' : '') }}"><a href="{{ url('admin') }}"><i class="fa fa-home"></i> <span>Página Inicial</span></a></li>
        @foreach($menus as $value)
        <li class="parent {{ (isset($menu_active[$value['id']])) ? $menu_active[$value['id']] : '' }}"><a href=""><i class="fa {{ $value['icon'] }}"></i> <span>{{ $value['name'] }}</span></a>
            <ul class="children">
                @foreach($value['sub_menus'] as $v)
                <li class="{{ (($v['route'] == $route) ? 'active' : '') }}"><a href="{{ url('admin/' . $v['route']) }}">{{ $v['name'] }}</a></li>
                @endforeach
            </ul>
        </li>
        @endforeach
    </ul>

</div><!-- leftpanel -->