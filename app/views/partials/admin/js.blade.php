<script src="{{ url('assets/js/admin/jquery-1.11.1.min.js') }}"></script>
<script type="text/javascript">
window.base = "{{ url('/') }}";
window.url = "{{ url('admin') }}";
</script>
<script src="{{ url('assets/js/admin/jquery-migrate-1.2.1.min.js') }}"></script>
<script src="{{ url('assets/js/admin/bootstrap.min.js') }}"></script>
<script src="{{ url('assets/js/admin/modernizr.min.js') }}"></script>
<script src="{{ url('assets/js/admin/pace.min.js') }}"></script>
<script src="{{ url('assets/js/admin/retina.min.js') }}"></script>
<script src="{{ url('assets/js/admin/jquery.cookies.js') }}"></script>

<script src="{{ url('assets/js/admin/toggles.min.js') }}"></script>
<script src="{{ url('assets/js/admin/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ url('assets/js/admin/jquery.mask.min.js') }}"></script>
<script src="{{ url('assets/js/admin/jquery.number.min.js') }}"></script>
<script src="{{ url('assets/js/admin/jquery.maskMoney.js') }}"></script>

<script src="{{ url('assets/js/admin/flot/jquery.flot.min.js') }}"></script>
<script src="{{ url('assets/js/admin/flot/jquery.flot.resize.min.js') }}"></script>
<script src="{{ url('assets/js/admin/flot/jquery.flot.spline.min.js') }}"></script>
<script src="{{ url('assets/js/admin/jquery.sparkline.min.js') }}"></script>
<script src="{{ url('assets/js/admin/morris.min.js') }}"></script>
<script src="{{ url('assets/js/admin/raphael-2.1.0.min.js') }}"></script>
<script src="{{ url('assets/js/admin/bootstrap-wizard.min.js') }}"></script>
<script src="{{ url('assets/js/admin/select2.min.js') }}"></script>

<script src="{{ url('assets/js/jquery.blockUI.js') }}"></script>
<script src="{{ url('assets/js/jquery.form.min.js') }}"></script>
<script src="{{ url('assets/js/jquery.noty.packaged.min.js') }}"></script>
<script src="{{ url('assets/js/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ url('assets/js/admin/jtable/jquery.jtable.js') }}"></script>

<script src="{{ url('assets/js/admin/custom.js') }}"></script>
<script src="{{ url('assets/js/admin/dashboard.js') }}"></script>
<script src="{{ url('assets/js/admin/scripts.js') }}"></script>