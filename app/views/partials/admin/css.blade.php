<link type="text/css" href="{{ url('assets/css/admin/style.default.css') }}" rel="stylesheet">
<link type="text/css" href="{{ url('assets/css/admin/bootstrap-timepicker.min.css') }}" rel="stylesheet">
<link type="text/css" href="{{ url('assets/css/admin/morris.css') }}" rel="stylesheet">
<link type="text/css" href="{{ url('assets/css/admin/select2.css') }}" rel="stylesheet" />
<link type="text/css" href="{{ url('assets/css/admin/loader-cerebelo.css') }}" rel="stylesheet" />
<link type="text/css" href="{{ url('assets/js/admin/jtable/themes/lightcolor/blue/jtable.css') }}" rel="stylesheet" />

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->