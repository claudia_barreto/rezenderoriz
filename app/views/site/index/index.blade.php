@section('content')

<!-- Banner -->
<div id="banner" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        @if(!empty($model))
        @foreach($model->images as $key => $value)
        @if($key == 0)
        <div class="item active" style="background-image:url('{{ $value->img }}');">
            <a href="">oi</a>
        </div>
        @else
        <div class="item" style="background-image:url('{{ $value->img }}');">
            <a href="">oi</a>
        </div>
        @endif
        @endforeach
        @endif
    </div>

    <!-- Indicators -->
    <ol class="carousel-indicators">
        @if(!empty($model))
        @foreach($model->images as $key => $value)
        @if($key == 0)
        <li data-target="#banner" data-slide-to="{{ $key }}" class="active"></li>
        @else
        <li data-target="#banner" data-slide-to="{{ $key }}"></li>
        @endif
        @endforeach
        @endif
    </ol>
</div>

<a href="{{ url($model->link_left) }}" title="Quem Somos" class="company"> 
    <img width="479" height="231" src="{{ $model->img_left }}">     
</a>

<a href="{{ url($model->link_right) }}" title="Portal do Cliente" class="doubts">
    <img width="479" height="231" src="{{ $model->img_right }}">
</a>

@stop