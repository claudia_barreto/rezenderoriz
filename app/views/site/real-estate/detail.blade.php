@section('content')
<ol class="breadcrumb">
    <li><a href="{{ url('projetos') }}">Projetos</a> > <a href="{{ url('projetos?c=' . ((!empty($model) && isset($model->category)) ? $model->category->id : "")) }}">{{ (!empty($model) && isset($model->category)) ? $model->category->title : "" }}</a> > {{ (!empty($model)) ? $model->name : "" }}</li>
</ol>

<div class="col-md-7 propertie-details-image">
    <div id="details" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            @if($model->images->count() > 0)
            @foreach($model->images as $key => $value)
            @if($key == 0)
            <li data-target="#details" data-slide-to="{{ $key }}" class="active"></li>
            @else
            <li data-target="#details" data-slide-to="{{ $key }}"></li>
            @endif
            @endforeach
            @endif
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            @if($model->images->count() > 0)
            @foreach($model->images as $key => $value)
            @if($key == 0)
            <div class="item active" style="background-image:url('{{ $value->img }}');">
            </div>
            @else
            <div class="item" style="background-image:url('{{ $value->img }}');"> 
            </div>
            @endif
            @endforeach
            @endif
        </div>      
    </div>
</div>

<div class="col-md-5">
    <div class="col-md-12">
        <h3>{{ ($model->count() > 0) ? $model->name : "" }}</h3>
    </div>

    <div class="col-md-7">
        <h6>Características do Empreendimento</h6>
        <p>{{ ($model->count() > 0) ? $model->description : "" }}</p>
    </div>

    <div class="col-md-5">
        <h6>Plantas do Projeto</h6>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h6 class="modal-title" id="myModalLabel">Plantas do Projeto</h6>
                    </div>
                    <div class="modal-body">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                @if($model->plants->count() > 0)
                                @foreach($model->plants as $key => $value)
                                <div class="item {{ ($key == 0) ? 'active' : '' }}">
                                    <div class="carousel-caption">
                                        <h3>{{ $value->description }}</h3>
                                    </div>
                                    <img src="{{ $value->img }}" alt="{{ $value->description }}">
                                </div>                                
                                @endforeach
                                @else
                                <div class="item active">
                                    <img src="http://placehold.it/199x140" alt="">                             
                                </div>
                                @endif
                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>                      
                </div>
            </div>
        </div>
        <a href="" data-toggle="modal" data-target="#myModal" title="Plantas"><img width="199" src="{{ ($model->plants->count() > 0) ? $model->plants[0]->img : "http://placehold.it/199x140" }}" alt="Plantas"></a>
    </div>

    <div class="clearfix"></div>

    @if($model->investments->count() > 0)
    <div class="col-md-12 investment" >
        <h3>Investimento</h3>
        <div>
            @foreach($model->investments as $value)
            <p>{{ $value->description }}</p>
            @endforeach
        </div>
        <input type="button" class="buy-button" value="Comprar"/>
    </div>
    @endif
</div>

@stop