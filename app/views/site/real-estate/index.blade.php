@section('content')

<ol class="breadcrumb">
    <li><a href="{{ url('projetos') }}">Projetos</a> > {{ $title }}</li>
</ol>

<ul class="properties-list">

    @if($model->count() > 0)
    @foreach($model as $value)
    <li>
        <a href="{{ url('projetos/detalhes/' . $value->id) }}" title="{{ $value->name }}">
            <div class="image-propertie" style="background-image:url('{{ isset($value->images[0]) ? $value->images[0]->img : "http://placehold.it/241x180" }}')"></div>
            <h4>{{ $value->name }}</h4>
            <span class="city"> {{ $value->neighborhood }} - {{ $value->city }}</span>
            <span class="rooms"> {{ $value->room }}</span>                
        </a>
    </li>
    @endforeach
    @endif

</ul>

@if($model->count() == 0)
<div class="clearfix"></div>
<p>Nenhum imóvel foi encontrado.</p>
@endif

<div class="clearfix"></div>

<nav>
    <ul class="pagination">
        {{ $model->links() }}
    </ul>
</nav>

@stop