@section('content')

<ol class="breadcrumb">
    <li><a href="{{ url('quem-somos') }}" title="Quem Somos">Quem Somos</a></li>
</ol>

<div class="col-md-4">
    <ul class="company-menu">
        <li><a href="{{ url('quem-somos') }}" title="Sobre a Empresa">Sobre a Empresa</a></li>
        <li><a href="{{ url('politica-privacidade') }}" class="active" title="Política de Privacidade">Política de Privacidade</a></li>
    </ul>
</div>

<div class="col-md-8">
    <h3>Política de Privacidade</h3>
    <p><a href="{{ url('politica-de-privacidade/download') }}">* Clique aqui para fazer download do Contrato de Política de Privacidade</a></p>
</div>

@stop