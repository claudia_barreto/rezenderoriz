@section('content')

<ol class="breadcrumb">
    <li><a href="{{ url('quem-somos') }}" title="Quem Somos">Quem Somos</a></li>
</ol>

<div class="col-md-4">
    <ul class="company-menu">
        <li><a href="{{ url('quem-somos') }}" class="active" title="Sobre a Empresa">Sobre a Empresa</a></li>
        <li><a href="{{ url('quem-somos?p=missao') }}" class="active" title="Missão">Missão</a></li>
        <li><a href="{{ url('quem-somos?p=visao') }}" class="active" title="Visão">Visão</a></li>
        <li><a href="{{ url('quem-somos?p=valores') }}" class="active" title="Valores">Valores</a></li>
        <!--<li><a href="{{ url('politica-privacidade') }}" title="Política de Privacidade">Política de Privacidade</a></li>-->
    </ul>
</div>

<div class="col-md-8">
    @if(!$page)
    <h3>{{ !empty($model) ? $model->title : "" }}</h3>
    <p>{{ !empty($model) ? $model->text : "" }}</p>
    @elseif($page == 'missao')    
    <h3>Missão</h3>
    <p>{{ !empty($model) ? $model->text_missao : "" }}</p>
    @elseif($page == 'visao')    
    <h3>Visão</h3>
    <p>{{ !empty($model) ? $model->text_visao : "" }}</p>
    @elseif($page == 'valores')
    <h3>Valores</h3>
    <p>{{ !empty($model) ? $model->text_valores : "" }}</p>
    @endif

    <div class="clearfix space20"></div>
    <h3>Empresas do Grupo</h3>
    <div class="row logos">
        @foreach($companies as $value)
        <div class="col-sm-3"><a target="_blank" href="{{ $value->link }}" title=""><img src="{{ url($value->img) }}"/></a></div>
        @endforeach
    </div>
    <h3>Principais Clientes</h3>
    <div class="row logos">
        @foreach($clients as $value)
        <div class="col-sm-3"><a target="_blank" href="{{ $value->link }}" title=""><img src="{{ url($value->img) }}"/></a></div>
        @endforeach
    </div>
</div>

@stop