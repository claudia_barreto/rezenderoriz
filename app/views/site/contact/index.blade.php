@section('content')

<ol class="breadcrumb">
    <li><a href="{{ url('contato') }}">Contato</a></li>
</ol>

<div class="col-md-6 contact-page">
    <div class="form-contact">

    <!--{{ Form::open(array('url' => 'contato/enviar', 'method' => 'post', 'id' => 'frm')) }}
    <div class="form-group">
        {{ Form::label('message_txt', 'Digite aqui sua mensagem', ['class' => 'control-label']) }}
        {{ Form::textarea('message_txt', null, ['class' => 'form-control', 'rows' => '2']) }}
    </div>

    <div class="form-group">
        {{ Form::label('name', 'Nome', ['class' => 'col-sm-2 control-label']) }}
        <div class="col-sm-10">
            {{ Form::text('name', null, ['class' => 'form-control']) }}
        </div>
        <div class="clearfix"></div>   
    </div>

    <div class="form-group">
        {{ Form::label('email', 'Email', ['class' => 'col-sm-2 control-label']) }}
        <div class="col-sm-10">
            {{ Form::email('email', null, ['class' => 'form-control']) }}
        </div>
        <div class="clearfix"></div>   
    </div>

    <div class="form-group">
        {{ Form::label('phone1', 'Telefone 1', ['class' => 'col-sm-6 control-label']) }}
        {{ Form::label('phone2', 'Telefone 2', ['class' => 'col-sm-6 control-label']) }}
        <div class="col-sm-6">
            <div class="col-md-2">
                {{ Form::text('ddd1', null, ['class' => 'form-control', 'maxlength' => '2']) }}
            </div>
            <div class="col-md-10">
                {{ Form::text('phone1', null, ['class' => 'form-control', 'maxlength' => '9']) }}
            </div>
        </div>
        <div class="col-sm-6">
            <div class="col-md-2">
                {{ Form::text('ddd2', null, ['class' => 'form-control', 'maxlength' => '2']) }}
            </div>
            <div class="col-md-10">
                {{ Form::text('phone2', null, ['class' => 'form-control', 'maxlength' => '9']) }}
            </div>              
        </div>  
        <div class="clearfix"></div>   
    </div>

    <div class="form-group">
        {{ Form::label('city', 'Cidade', ['class' => 'col-sm-2 control-label']) }}
        <div class="col-sm-4">
            {{ Form::text('city', null, ['class' => 'form-control']) }}
        </div>
        {{ Form::label('city', 'UF', ['class' => 'col-sm-1 control-label']) }}
        <div class="col-sm-3">
            {{ Form::text('state', null, ['class' => 'form-control', 'maxlength' => '2']) }}
        </div>
        {{ Form::submit('Enviar', ['class' => 'btn btn-default col-md-2 send-contact']) }}
    </div>            
    {{ Form::close() }}
    <div id="loadRezende" style="display: none;">        
        <div id="loadCerebelo"><img src="{{ url('assets/img/site/favicon.png') }}" ></div>
    </div>-->
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.3486015798217!2d-43.1818338!3d-22.9005095!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x997f5c7dd5bb61%3A0x2efd22af96874d35!2sR.+Acre%2C+83+-+Centro%2C+Rio+de+Janeiro+-+RJ!5e0!3m2!1spt-BR!2sbr!4v1439320949863" frameborder="0" style="border:0; width:100%; height:380px;" allowfullscreen></iframe>
        <div class="info">
                <span class="tel">{{ $tel }}</span>
                <span class="face"> <img alt="Facebook Rezende Roriz" src="{{ url('assets/img/site/face.png') }}"> <a title="Facebook Rezende Roriz" target="_blank" href="{{ $face }}">facebook.com/RezendeRoriz</a></span>
                <span class="address">{{ $address }}</span>
        </div>
    </div>
    <div class="fb-page" data-href="https://www.facebook.com/RezendeRoriz" data-width="280" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/RezendeRoriz"><a href="https://www.facebook.com/RezendeRoriz">Rezende Roriz</a></blockquote></div></div>
    <div class="clearfix space20"></div>
</div>

<div class="col-md-6 mail-image">
    <img src="{{ url("assets/img/site/contact.png") }}"/>
</div>

@stop

@section('js')
<script src="{{ url('assets/js/site/contact/form.js') }}"></script>
@stop