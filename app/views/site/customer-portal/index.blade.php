@section('content')

<ol class="breadcrumb">
    <li><a href="{{ url('portal-do-cliente') }}">Portal do Cliente</a></li>
</ol>

<div class="col-md-6 form-contact">            

    {{ Form::open(array('url' => 'portal-do-cliente/enviar', 'method' => 'post', 'id' => 'frm')) }}
    <div class="form-group">
        {{ Form::label('message_txt', 'Digite aqui sua mensagem', ['class' => 'control-label']) }}
        {{ Form::textarea('message_txt', null, ['class' => 'form-control', 'rows' => '2']) }}
    </div>

    <div class="form-group">
        {{ Form::label('name', 'Nome', ['class' => 'col-sm-2 control-label']) }}
        <div class="col-sm-10">
            {{ Form::text('name', null, ['class' => 'form-control']) }}
        </div>
        <div class="clearfix"></div>   
    </div>

    <div class="form-group">
        {{ Form::label('email', 'Email', ['class' => 'col-sm-2 control-label']) }}
        <div class="col-sm-10">
            {{ Form::email('email', null, ['class' => 'form-control']) }}
        </div>
        <div class="clearfix"></div>   
    </div>

    <div class="form-group">
        {{ Form::label('phone1', 'Telefone 1', ['class' => 'col-sm-6 control-label']) }}
        {{ Form::label('phone2', 'Telefone 2', ['class' => 'col-sm-6 control-label']) }}
        <div class="col-sm-6">
            <div class="col-md-2">
                {{ Form::text('ddd1', null, ['class' => 'form-control', 'maxlength' => '2']) }}
            </div>
            <div class="col-md-10">
                {{ Form::text('phone1', null, ['class' => 'form-control', 'maxlength' => '9']) }}
            </div>
        </div>
        <div class="col-sm-6">
            <div class="col-md-2">
                {{ Form::text('ddd2', null, ['class' => 'form-control', 'maxlength' => '2']) }}
            </div>
            <div class="col-md-10">
                {{ Form::text('phone2', null, ['class' => 'form-control', 'maxlength' => '9']) }}
            </div>              
        </div>  
        <div class="clearfix"></div>   
    </div>

    <div class="form-group">
        {{ Form::label('city', 'Cidade', ['class' => 'col-sm-2 control-label']) }}
        <div class="col-sm-4">
            {{ Form::text('city', null, ['class' => 'form-control']) }}
        </div>
        {{ Form::label('city', 'UF', ['class' => 'col-sm-1 control-label']) }}
        <div class="col-sm-3">
            {{ Form::text('state', null, ['class' => 'form-control', 'maxlength' => '2']) }}
        </div>
        {{ Form::submit('Enviar', ['class' => 'btn btn-default col-md-2 send-contact']) }}
    </div>            
    {{ Form::close() }}
    <div id="loadRezende" style="display: none;">        
        <div id="loadCerebelo"><img src="{{ url('assets/img/site/favicon.png') }}" ></div>
    </div>
</div>

<div class="col-md-6 contact-image">
    <div class="finish"></div>
</div>

@stop

@section('js')
<script src="{{ url('assets/js/site/customer-portal/form.js') }}"></script>
@stop