@section('content')
<div class="panel panel-signin">
    <div class="panel-body">
        <div class="logo text-center">
            <img src="{{ url('assets/img/admin/logo.png') }}" alt="Logo" >
        </div>
        <br />
        <h4 class="text-center mb5">Recuperar Senha</h4>
        <div class="mb30"></div>

        {{ Form::open(array('url' => 'admin/enviar-senha', 'method' => 'post', 'id' => 'form-forgot')) }}
        <div class="input-group mb15">
            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
            {{ Form::email('email', '', [
                'class' => 'form-control',
                'placeholder' => 'E-mail',
                'required',
                'autofocus'
            ]) }}
        </div><!-- input-group -->

        <div class="clearfix">
            <div class="pull-left">
                <a href="{{ url('admin/login') }}" class="btn btn-info"><i class="fa fa-angle-left ml5"></i> Ir para Login</a>
            </div>
            <div class="pull-right">
                <button type="submit" class="btn btn-success">Recuperar <i class="fa fa-angle-right ml5"></i></button>
            </div>
        </div>                      
        {{ Form::close() }}

    </div><!-- panel-body -->
    <div class="panel-footer">
        <div class="logo text-center">
            <img src="{{ url('assets/img/admin/logo-cerebelo.png') }}" alt="Logo" >
        </div>
    </div>
</div><!-- panel -->
@stop

@section('js')
<script src="{{ url('assets/js/admin/forgot/index.js') }}"></script>
@stop