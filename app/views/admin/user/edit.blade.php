@section('content')

<div class="panel">    
    <a href="{{ url('admin/usuarios') }}" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i> Voltar</a>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Cadastro</h4>
        <p class="text-info">(*) campos obrigatórios</p>
    </div><!-- panel-heading -->

    <div class="panel-body nopadding">

        <br>
        {{ Form::model($model, array('url' => 'admin/usuarios/salvar', 'method' => 'post', 'id' => 'form', 'class' => 'form-horizontal')) }}
        {{ Form::hidden('id', null, ['id' => 'id']) }}

        <div class="form-group">
            {{ Form::label('group_id', 'Grupo (*)', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-2">
                {{ Form::select('group_id', ['' => 'Selecione'] + $groups, null, [
                    'data-placeholder' => 'Selecione',
                    'class' => 'width100p'
                ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('name', 'Nome (*)', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-4">
                {{ Form::text('name', null, [
                        'maxlength' => '100',
                        'class' => 'form-control'
                    ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('avatar', 'Foto', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-4">
                {{ Form::file('avatar', null, [
                        'avatar' => 'form-control',
                        'class' => 'form-control'
                    ]) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('', '', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-4">
                <img id="preview-photo" width="60" src="{{ $model->photo }}"/>
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('email', 'E-mail', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-4">
                <div class="input-group mb15">
                    <span class="input-group-addon">@</span>
                    {{ Form::email('email', null, [
                            'maxlength' => '100',
                            'class' => 'form-control'
                        ]) }}
                </div>
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('username', 'Usuário (*)', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-2">
                {{ Form::text('username', null, [
                        'maxlength' => '20',
                        'class' => 'form-control'
                    ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('password', 'Senha (*)', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-2">
                {{ Form::password('password', [
                        'maxlength' => '20',
                        'class' => 'form-control'
                    ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('password_confirm', 'Confirmar Senha (*)', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-2">
                {{ Form::password('password_confirm', [
                        'maxlength' => '20',
                        'class' => 'form-control'
                    ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('active', 'Ativar? (*)', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-2">
                {{ Form::select('active', [
                    '' => 'Selecione',
                    'Y' => 'Sim',
                    'N' => 'Não'
                ], null, [
                    'data-placeholder' => 'Selecione',
                    'class' => 'width100p'
                ]) }}
            </div>
        </div>

        <div class="panel-footer">
            <div class="row">
                <div class="col-sm-2 col-sm-offset-2">
                    {{ Form::submit('Salvar', ['class' => 'btn btn-primary mr5']) }}
                    {{ Form::reset('Limpar', ['class' => 'btn btn-dark']) }}
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div><!-- panel-body -->
</div>

@stop

@section('js')
<script src="{{ url('assets/js/admin/user/form.js') }}"></script>
@stop