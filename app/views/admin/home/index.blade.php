@section('content')

<div class="panel">    
    <a href="{{ url('admin/home') }}" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i> Atualizar Página</a>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Cadastro</h4>
        <p class="text-info">(*) campos obrigatórios</p>
    </div><!-- panel-heading -->

    <div class="panel-body nopadding">

        <br>
        <div class="col-sm-12">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-primary">
                <li class="active"><a href="#home1" data-toggle="tab"><strong>Dados</strong></a></li>
                <li><a href="#home2" data-toggle="tab"><strong>Imagens do Banner</strong></a></li>
                <li><a href="#home3" data-toggle="tab"><strong>Imagem da Esquerda</strong></a></li>
                <li><a href="#home4" data-toggle="tab"><strong>Imagem da Direita</strong></a></li>
            </ul>

            {{ Form::model($model, array('url' => 'admin/home/salvar', 'method' => 'post', 'id' => 'form', 'class' => 'form-horizontal', 'files' => true)) }}
            {{ Form::hidden('id', null, ['id' => 'id']) }}

            <!-- Tab panes -->
            <div class="tab-content tab-content-primary mb30">
                <div class="tab-pane active" id="home1">
                    <div class="form-group">
                        {{ Form::label('phone', 'Matriz Telefone (*)', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-2">
                            {{ Form::text('phone', null, [
                        'maxlength' => '14',
                        'class' => 'form-control phone'
                    ]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('facebook', 'Facebook (*)', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-4">
                            {{ Form::text('facebook', null, [
                        'maxlength' => '250',
                        'class' => 'form-control'
                    ]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('logradouro', 'Matriz Logradouro (*)', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-4">
                            {{ Form::text('logradouro', null, [
                        'maxlength' => '250',
                        'class' => 'form-control'
                    ]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('number', 'Matriz Número (*)', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-1">
                            {{ Form::text('number', null, [
                        'maxlength' => '10',
                        'class' => 'form-control'
                    ]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('complement', 'Matriz Complemento', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-1">
                            {{ Form::text('complement', null, [
                        'maxlength' => '10',
                        'class' => 'form-control'
                    ]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('neighborhood', 'Matriz Bairro (*)', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-3">
                            {{ Form::text('neighborhood', null, [
                        'maxlength' => '150',
                        'class' => 'form-control'
                    ]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('city', 'Matriz Cidade (*)', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-3">
                            {{ Form::text('city', null, [
                        'maxlength' => '150',
                        'class' => 'form-control'
                    ]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('state', 'Matriz Estado (*)', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-1">
                            {{ Form::text('state', null, [
                        'maxlength' => '2',
                        'class' => 'form-control'
                    ]) }}
                        </div>
                    </div>
                    
                    <div class="form-group">
                        {{ Form::label('phone', 'Filial Telefone', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-2">
                            {{ Form::text('filial_phone', null, [
                        'maxlength' => '14',
                        'class' => 'form-control phone'
                    ]) }}
                        </div>
                    </div>
                    
                    <div class="form-group">
                        {{ Form::label('logradouro', 'Filial Logradouro', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-4">
                            {{ Form::text('filial_logradouro', null, [
                        'maxlength' => '250',
                        'class' => 'form-control'
                    ]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('number', 'Filial Número', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-1">
                            {{ Form::text('filial_number', null, [
                        'maxlength' => '10',
                        'class' => 'form-control'
                    ]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('complement', 'Filial Complemento', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-1">
                            {{ Form::text('filial_complement', null, [
                        'maxlength' => '10',
                        'class' => 'form-control'
                    ]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('neighborhood', 'Filial Bairro', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-3">
                            {{ Form::text('filial_neighborhood', null, [
                        'maxlength' => '150',
                        'class' => 'form-control'
                    ]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('city', 'Filial Cidade', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-3">
                            {{ Form::text('filial_city', null, [
                        'maxlength' => '150',
                        'class' => 'form-control'
                    ]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('state', 'Filial Estado', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-1">
                            {{ Form::text('filial_state', null, [
                        'maxlength' => '2',
                        'class' => 'form-control'
                    ]) }}
                        </div>
                    </div>
                </div><!-- tab-pane -->

                <div class="tab-pane" id="home2">
                    <div class="form-group">
                        {{ Form::label('images', 'Imagens do Banner', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-4">
                            {{ Form::file('images[]', [
                        'multiple' => true,
                        'class' => 'form-control'
                    ]) }}
                        </div>
                    </div>
                    <div class="row media-manager">                        
                        @foreach($model->images as $value)
                        <div class="col-xs-6 col-sm-4 col-md-5 document">
                            <div class="thmb">
                                <div class="btn-group fm-group">
                                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle fm-toggle" type="button">
                                        <span class="caret"></span>
                                    </button>
                                    <ul role="menu" class="dropdown-menu fm-menu pull-right">
                                        <li><a class="delete-images" href="{{ url('admin/home/excluir-banner/' . $value->id) }}"><i class="fa fa-trash-o"></i> Excluir</a></li>
                                    </ul>
                                </div><!-- btn-group -->
                                <div class="thmb-prev">
                                    <img alt="" class="img-responsive" src="{{ $value->img }}">
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div><!-- tab-pane -->

                <div class="tab-pane" id="home3">
                    <div class="form-group">
                        {{ Form::label('image_left', 'Imagem da Esquerda', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-4">
                            {{ Form::file('image_left', [
                        'class' => 'form-control'
                    ]) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('link_left', 'Link (*)', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-4">
                            {{ Form::text('link_left', null, [
                                'maxlength' => '250',
                                'class' => 'form-control'
                            ]) }}
                        </div>
                    </div>
                    <div class="row media-manager">                        
                        {{ Form::label('', '*', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-xs-6 col-sm-4 col-md-3 document">
                            <div class="thmb">
                                <div class="thmb-prev">
                                    <img alt="" class="img-responsive" src="{{ $model->img_left }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- tab-pane -->

                <div class="tab-pane" id="home4">
                    <div class="form-group">
                        {{ Form::label('image_right', 'Imagem da Direita', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-4">
                            {{ Form::file('image_right', [
                        'class' => 'form-control'
                    ]) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('link_right', 'Link (*)', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-4">
                            {{ Form::text('link_right', null, [
                                'maxlength' => '250',
                                'class' => 'form-control'
                            ]) }}
                        </div>
                    </div>
                    <div class="row media-manager">                        
                        {{ Form::label('', '*', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-xs-6 col-sm-4 col-md-3 document">
                            <div class="thmb">
                                <div class="thmb-prev">
                                    <img alt="" class="img-responsive" src="{{ $model->img_right }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- tab-pane -->
            </div><!-- tab-content -->

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-3">
                        {{ Form::submit('Salvar', ['class' => 'btn btn-primary mr5']) }}
                        {{ Form::reset('Limpar', ['class' => 'btn btn-dark']) }}
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div><!-- panel-body -->
</div>

@stop

@section('js')
<script src="{{ url('assets/js/admin/home/form.js') }}"></script>
<script src="{{ url('assets/js/admin/home/index.js') }}"></script>
@stop