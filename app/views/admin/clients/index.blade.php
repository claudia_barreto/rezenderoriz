@section('content')

<div class="panel">    
    <a href="{{ url('admin/principais-clientes') }}" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i> Atualizar Página</a>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Cadastro</h4>
        <p class="text-info">(*) campos obrigatórios</p>
    </div><!-- panel-heading -->

    <div class="panel-body nopadding">

        <br>
        <div class="col-sm-12">
            {{ Form::open(array('url' => 'admin/principais-clientes/salvar', 'method' => 'post', 'id' => 'form', 'class' => 'form-horizontal', 'files' => true)) }}
            <div class="form-group">
                {{ Form::label('image', 'Imagem', ['class' => 'col-sm-1 control-label']) }}
                <div class="col-sm-4">
                    {{ Form::file('image', [
                        'class' => 'form-control'
                    ]) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('link', 'Link', ['class' => 'col-sm-1 control-label']) }}
                <div class="col-sm-4">
                    {{ Form::text('link', null, [
                        'maxlength' => '250',
                        'class' => 'form-control'
                    ]) }}
                </div>
            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-3">
                        {{ Form::submit('Salvar', ['class' => 'btn btn-primary mr5']) }}
                        {{ Form::reset('Limpar', ['class' => 'btn btn-dark']) }}
                    </div>
                </div>
            </div>
            {{ Form::close() }}

            <div class="row media-manager">                        
                @foreach($model as $value)
                <div class="col-xs-6 col-sm-4 col-md-5 document">
                    <div class="thmb">
                        <div class="btn-group fm-group">
                            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle fm-toggle" type="button">
                                <span class="caret"></span>
                            </button>
                            <ul role="menu" class="dropdown-menu fm-menu pull-right">
                                <li><a class="delete-images" href="{{ url('admin/principais-clientes/excluir/' . $value->id) }}"><i class="fa fa-trash-o"></i> Excluir</a></li>
                                <li><a class="edit-images" href="{{ url('admin/principais-clientes/editar/' . $value->id) }}"><i class="fa fa-trash-o"></i> Salvar</a></li>
                            </ul>
                        </div><!-- btn-group -->
                        <div class="thmb-prev">
                            <img width="130" height="130" alt="" class="img-responsive" src="{{ $value->img }}">
                        </div>
                        <h5 class="fm-title">
                            {{ Form::text('link', $value->link, [
                                'maxlength' => '250',
                                'id' => 'link',
                                'class' => 'form-control'
                            ]) }}                
                        </h5>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div><!-- panel-body -->
</div>

@stop

@section('js')
<script src="{{ url('assets/js/admin/clients/form.js') }}"></script>
<script src="{{ url('assets/js/admin/clients/index.js') }}"></script>
@stop