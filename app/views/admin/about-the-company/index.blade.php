@section('content')

<div class="panel">    
    <a href="{{ url('admin/sobre-a-empresa') }}" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i> Atualizar Página</a>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Cadastro</h4>
        <p class="text-info">(*) campos obrigatórios</p>
    </div><!-- panel-heading -->

    <div class="panel-body nopadding">

        <br>
        {{ Form::model($model, array('url' => 'admin/sobre-a-empresa/salvar', 'method' => 'post', 'id' => 'form', 'class' => 'form-horizontal', 'files' => true)) }}
        {{ Form::hidden('id', null, ['id' => 'id']) }}

        <div class="form-group">
            {{ Form::label('text', 'Título (*)', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-4">
                {{ Form::text('title', null, [
                        'class' => 'form-control'
                    ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('text', 'Texto (*)', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-4">
                {{ Form::textarea('text', null, [
                        'rows' => '10',
                        'class' => 'form-control'
                    ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('text', 'Texto de Missão (*)', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-4">
                {{ Form::textarea('text_missao', null, [
                        'rows' => '10',
                        'class' => 'form-control'
                    ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('text', 'Texto de Visão (*)', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-4">
                {{ Form::textarea('text_visao', null, [
                        'rows' => '10',
                        'class' => 'form-control'
                    ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('text', 'Texto de Valores (*)', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-4">
                {{ Form::textarea('text_valores', null, [
                        'rows' => '10',
                        'class' => 'form-control'
                    ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('image', 'Imagem', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-4">
                {{ Form::file('image', null, [
                        'id' => 'image',
                        'class' => 'form-control'
                    ]) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('', '', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-6">
                <img id="preview-image" src="{{ (isset($model->image) && !empty($model->image)) ? $model->img : 'http://placehold.it/578x441' }}"/>
            </div>
        </div>

        <div class="panel-footer">
            <div class="row">
                <div class="col-sm-2 col-sm-offset-2">
                    {{ Form::submit('Salvar', ['class' => 'btn btn-primary mr5']) }}
                    {{ Form::reset('Limpar', ['class' => 'btn btn-dark']) }}
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div><!-- panel-body -->
</div>

@stop

@section('js')
<script src="{{ url('assets/js/admin/about-the-company/form.js') }}"></script>
@stop