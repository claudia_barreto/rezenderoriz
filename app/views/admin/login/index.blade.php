@section('content')
<div class="panel panel-signin">
    <div class="panel-body">
        <div class="logo text-center">
            <img src="{{ url('assets/img/admin/logo-login.png') }}" alt="Logo" >
        </div>
        <br />
        <h4 class="text-center mb5">Login</h4>
        <div class="mb30"></div>

        {{ Form::open(array('url' => 'admin/login', 'method' => 'post', 'id' => 'form-login')) }}
        <div class="input-group mb15">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            {{ Form::text('username', '', [
                'class' => 'form-control',
                'placeholder' => 'Usuário',
                'required',
                'autofocus'
            ]) }}
        </div><!-- input-group -->
        <div class="input-group mb15">
            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
            {{ Form::password('password', [
                'class' => 'form-control',
                'placeholder' => 'Senha',
                'required'
            ]) }}
        </div><!-- input-group -->

        <div class="clearfix">
            <a href="{{ url('admin/esqueci-senha') }}">Esqueci minha senha!</a>
        </div>

        <div class="clearfix">
            <div class="pull-left">
                <div class="ckbox ckbox-primary mt10">
                    {{ Form::checkbox('remember', '1', false, ['id' => 'rememberMe']) }}
                    <label for="rememberMe">Lembrar</label>
                </div>
            </div>
            <div class="pull-right">
                <button type="submit" class="btn btn-success">Entrar <i class="fa fa-angle-right ml5"></i></button>
            </div>
        </div>                      
        {{ Form::close() }}

    </div><!-- panel-body -->
    <div class="panel-footer">
        <div class="logo text-center">
            <img src="{{ url('assets/img/admin/logo-cerebelo.png') }}" alt="Logo" >
        </div>
    </div>
</div><!-- panel -->
@stop

@section('js')
<script src="{{ url('assets/js/admin/login/index.js') }}"></script>
@stop