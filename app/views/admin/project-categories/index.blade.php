@section('content')

<div class="panel">    
    <a href="{{ url('admin/categoria-projetos/novo') }}" class="btn btn-default"><i class="glyphicon glyphicon-plus"></i> Novo</a>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a title="" data-toggle="tooltip" class="panel-minimize tooltips maximize" href="" data-original-title="Exibir"><i class="fa fa-plus"></i></a>
        </div><!-- panel-btns -->
        <h3 class="panel-title">Filtros</h3>
    </div>
    <div class="panel-body" style="display: none;">
        {{ Form::open(array('url' => 'admin/categoria-projetos/listagem', 'method' => 'post', 'id' => 'form', 'class' => 'form-horizontal')) }}

        
        {{ Form::close() }}
    </div>
</div>

<div class="panel panel-default">
    <div id="jtable"></div>
</div>

@stop

@section('js')
<script src="{{ url('assets/js/admin/project-categories/jtable.js') }}"></script>
@stop