@section('content')

<div class="panel">    
    <a href="{{ url('admin/categoria-projetos') }}" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i> Voltar</a>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Cadastro</h4>
        <p class="text-info">(*) campos obrigatórios</p>
    </div><!-- panel-heading -->

    <div class="panel-body nopadding">

        <br>
        <div class="col-sm-12">
            {{ Form::model($model, array('url' => 'admin/categoria-projetos/salvar', 'method' => 'post', 'id' => 'form', 'class' => 'form-horizontal', 'files' => true)) }}
            {{ Form::hidden('id', null, ['id' => 'id']) }}

            <div class="form-group">
                {{ Form::label('name', 'Título (*)', ['class' => 'col-sm-2 control-label']) }}
                <div class="col-sm-4">
                    {{ Form::text('title', null, [
                        'maxlength' => '150',
                        'class' => 'form-control'
                    ]) }}
                </div>
            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-3">
                        {{ Form::submit('Salvar', ['class' => 'btn btn-primary mr5']) }}
                        {{ Form::reset('Limpar', ['class' => 'btn btn-dark']) }}
                    </div>
                </div>
            </div>
            
            {{ Form::close() }}
        </div>
    </div><!-- panel-body -->
</div>

@stop

@section('js')
<script src="{{ url('assets/js/admin/project-categories/form.js') }}"></script>
@stop