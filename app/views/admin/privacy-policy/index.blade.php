@section('content')

<div class="panel">    
    <a href="{{ url('admin/politica-de-privacidade') }}" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i> Atualizar Página</a>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Cadastro</h4>
        <p class="text-info">(*) campos obrigatórios</p>
    </div><!-- panel-heading -->

    <div class="panel-body nopadding">

        <br>
        {{ Form::open(array('url' => 'admin/politica-de-privacidade/salvar', 'method' => 'post', 'id' => 'form', 'class' => 'form-horizontal', 'files' => true)) }}

        <div class="form-group">
            {{ Form::label('file', 'Arquivo (*)', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-4">
                {{ Form::file('file', [
                        'class' => 'form-control'
                    ]) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('', '', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-4">
                <a href="{{ url('politica-de-privacidade/download') }}">Baixar Política de Privacidade</a>
            </div>
        </div>

        <div class="panel-footer">
            <div class="row">
                <div class="col-sm-2 col-sm-offset-2">
                    {{ Form::submit('Salvar', ['class' => 'btn btn-primary mr5']) }}
                    {{ Form::reset('Limpar', ['class' => 'btn btn-dark']) }}
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div><!-- panel-body -->
</div>

@stop

@section('js')
<script src="{{ url('assets/js/admin/privacy-policy/form.js') }}"></script>
@stop