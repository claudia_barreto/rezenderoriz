@section('content')

<div class="panel">    
    <a href="{{ url('admin/projetos') }}" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i> Voltar</a>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Cadastro</h4>
        <p class="text-info">(*) campos obrigatórios</p>
    </div><!-- panel-heading -->

    <div class="panel-body nopadding">

        <br>
        <div class="col-sm-12">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-primary">
                <li class="active"><a href="#home1" data-toggle="tab"><strong>Imóvel</strong></a></li>
                <li><a href="#home2" data-toggle="tab"><strong>Imagens</strong></a></li>
                <li><a href="#home3" data-toggle="tab"><strong>Plantas do Projeto</strong></a></li>
                <li><a href="#home4" data-toggle="tab"><strong>Investimento</strong></a></li>
            </ul>

            {{ Form::model($model, array('url' => 'admin/projetos/salvar', 'method' => 'post', 'id' => 'form', 'class' => 'form-horizontal', 'files' => true)) }}
            {{ Form::hidden('id', null, ['id' => 'id']) }}

            <!-- Tab panes -->
            <div class="tab-content tab-content-primary mb30">
                <div class="tab-pane active" id="home1">
                    <div class="form-group">
                        {{ Form::label('project_categories_id', 'Categoria (*)', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-2">
                            {{ Form::select('project_categories_id', ['' => 'Selecione'] + $categories, null, [
                                'data-placeholder' => 'Selecione',
                                'class' => 'width100p'
                            ]) }}
                        </div>
                    </div>
                    
                    <div class="form-group">
                        {{ Form::label('name', 'Nome (*)', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-4">
                            {{ Form::text('name', null, [
                        'maxlength' => '150',
                        'class' => 'form-control'
                    ]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('description', 'Descrição (*)', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-4">
                            {{ Form::textarea('description', null, [
                                'maxlength' => '250',
                                'rows' => '5',
                                'class' => 'form-control'
                            ]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('neighborhood', 'Bairro (*)', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-3">
                            {{ Form::text('neighborhood', null, [
                                'maxlength' => '100',
                                'class' => 'form-control'
                            ]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('city', 'Cidade (*)', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-3">
                            {{ Form::text('city', null, [
                                'maxlength' => '100',
                                'class' => 'form-control'
                            ]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('room', 'Quartos', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-3">
                            {{ Form::text('room', null, [
                                'maxlength' => '100',
                                'placeholder' => 'exemplo: 2 a 3 quartos',
                                'class' => 'form-control'
                            ]) }}
                        </div>
                    </div>
                </div><!-- tab-pane -->

                <div class="tab-pane" id="home2">
                    <div class="form-group">
                        {{ Form::label('images', 'Imagens', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-4">
                            {{ Form::file('images[]', [
                                'multiple' => true,
                                'class' => 'form-control'
                            ]) }}
                        </div>
                    </div>
                    <div class="row media-manager">
                        @foreach($model->images as $value)
                        {{ Form::hidden('real_estate_images_id[]', $value->id) }}
                        <div class="col-xs-6 col-sm-4 col-md-3 document">
                            <div class="thmb">
                                <div class="btn-group fm-group">
                                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle fm-toggle" type="button">
                                        <span class="caret"></span>
                                    </button>
                                    <ul role="menu" class="dropdown-menu fm-menu pull-right">
                                        <li><a class="delete-images" href="{{ url('admin/projetos/excluir-imagem/' . $value->id) }}"><i class="fa fa-trash-o"></i> Excluir</a></li>
                                    </ul>
                                </div><!-- btn-group -->
                                <div class="thmb-prev">
                                    <img alt="" class="img-responsive" src="{{ $value->img }}">
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div><!-- tab-pane -->

                <div class="tab-pane" id="home3">
                    <div class="form-group">
                        {{ Form::label('plants', 'Imagens', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-4">
                            {{ Form::file('plants', [
                                'multiple' => true,
                                'class' => 'form-control'
                            ]) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('description', 'Descrição ', ['class' => 'col-sm-2 control-label']) }}
                        <div class="col-sm-4">
                            {{ Form::textarea('description_plants', null, [
                                'maxlength' => '250',
                                'rows' => '2',
                                'class' => 'form-control'
                            ]) }}
                        </div>
                    </div>
                    <div class="row media-manager">
                        @foreach($model->plants as $value)
                        {{ Form::hidden('real_estate_plants_id[]', $value->id) }}
                        <div class="col-xs-6 col-sm-4 col-md-3 document">
                            <div class="thmb">
                                <div class="btn-group fm-group">
                                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle fm-toggle" type="button">
                                        <span class="caret"></span>
                                    </button>
                                    <ul role="menu" class="dropdown-menu fm-menu pull-right">
                                        <li><a class="delete-images" href="{{ url('admin/projetos/excluir-imagem/' . $value->id) }}"><i class="fa fa-trash-o"></i> Excluir</a></li>
                                        <li><a class="save-updates" href="{{ url('admin/projetos/salvar-alteracoes/' . $value->id) }}"><i class="fa fa-save"></i> Salvar Alterações</a></li>
                                    </ul>
                                </div><!-- btn-group -->
                                <div class="thmb-prev">
                                    <img alt="" class="img-responsive" src="{{ $value->img }}">
                                    {{ Form::label('description_plants', 'Descrição:', ['class' => 'col-sm-2 control-label']) }}
                                    {{ Form::textarea('description_plants2', $value->description, [
                                        'maxlength' => '250',
                                        'id' => 'description_plants2',
                                        'rows' => '2',
                                        'class' => 'form-control'
                                    ]) }}
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div><!-- tab-pane -->

                <div class="tab-pane" id="home4">
                    <div id="list-investment">
                        @foreach($model->investments as $value)
                        {{ Form::hidden('real_estate_investment_id[]', $value->id) }}
                        <div class="form-group">
                            {{ Form::label('description_i', 'Descrição', ['class' => 'col-sm-2 control-label']) }}
                            <div class="col-sm-4">
                                {{ Form::text('description_i[]', $value->description, [
                                'placeholder' => 'exemplo: 1 x R$ 600.000,00 = R$600.000,00',
                                'class' => 'form-control'
                            ]) }}
                            </div>
                            <div class="col-sm-1">
                                <button href="{{ url('admin/projetos/excluir-investimento/' . $value->id) }}" class="btn btn-danger delete-investment"><i class="glyphicon glyphicon-plus"></i> Excluir</button>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="form-group">
                        {{ Form::label('', '', ['class' => 'col-sm-2 control-label']) }}
                        <button href="#" id="add-investment" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Adicionar</button>
                    </div>
                </div><!-- tab-pane -->
            </div><!-- tab-content -->

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-3">
                        {{ Form::submit('Salvar', ['class' => 'btn btn-primary mr5']) }}
                        {{ Form::reset('Limpar', ['class' => 'btn btn-dark']) }}
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div><!-- panel-body -->
</div>

@stop

@section('js')
<script src="{{ url('assets/js/admin/real-estate/form.js') }}"></script>
<script src="{{ url('assets/js/admin/real-estate/index.js') }}"></script>
@stop