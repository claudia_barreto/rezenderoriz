<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Rezende Roriz | Incorporação e Construção</title> 

    </head>

    <body>
        <table style="width:600px; background:#efefef; color:#27274f ; font: 14px Arial; border:1px solid #971b1d; padding:0px;">
            <tr>
                <td  colspan="2">
                    <h1 style="color:#971b1d;  padding:20px; border-bottom:1px solid #971b1d">Portal do Cliente</h1>
                </td>
            </tr> 
            <tr>
                <td style="width:30%; padding:5px 20px; font-weight: bold;">Mensagem: </td>
                <td>{{ $message_txt }}</td>
            </tr>
            <tr>
                <td style="width:30%;padding:5px 20px;font-weight: bold;">Nome: </td>
                <td>{{ $name }}</td>
            </tr>
            <tr>
                <td style="width:30%; padding:5px 20px;font-weight: bold;">Email: </td>
                <td>{{ $email }}</td>
            </tr>
            <tr>
                <td style="width:30%;padding:5px 20px;font-weight: bold;">Telefone 1: </td>
                <td>({{ $ddd1 }}) {{ $phone1 }}</td>
            </tr>
            <tr>
                <td style="width:30%;padding:5px 20px;font-weight: bold;">Telefone 2: </td>
                <td>({{ $ddd2 }}) {{ $phone2 }}</td>
            </tr>
            <tr>
                <td style="width:30%;padding:5px 20px;font-weight: bold;">Cidade: </td>
                <td>{{ $city }}</td>
            </tr>
            <tr>
                <td style="width:30%;padding:5px 20px;font-weight: bold;">UF: </td>
                <td>{{ $state }}</td>
            </tr>
            <tr><td><br><br></td></tr>
            <tr style="background:#971b1d;margin-top:20px;">
                <td  colspan="2" style="padding:20px 20px; text-align:right;">
                    <img src="{{ $url }}" style="width:20%"/>
                </td>
            </tr> 
        </table>

    </body>
</html>