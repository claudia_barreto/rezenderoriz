
function replaceAll(str, de, para) {
    var pos = str.indexOf(de);
    while (pos > -1) {
        str = str.replace(de, para);
        pos = str.indexOf(de);
    }
    return (str);
}

$(document).ready(function () {
    $('.decimal').maskMoney({prefix: '', allowNegative: true, thousands: '.', decimal: ',', affixesStay: false});
    $('.integer').mask('00000000000#');
    $('.date').datepicker();
    $(".date").mask("99/99/9999");
    $(".phone").mask("(99) 9999-9999");

});

$(document).ajaxStart(function () {
    $.blockUI({message: '<div id="loadCerebelo"><img src="' + window.base + '/assets/img/loaders/logo_cerebelo.png") }}" ></div>', css: {border: 'none'}});
});

$(document).ajaxStop(function () {
    $("table a.delete").off("click").on("click", function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        noty({
            layout: 'center',
            modal: true,
            text: 'Deseja realmente excluir o registro?',
            buttons: [
                {addClass: 'btn btn-primary', text: 'Sim', onClick: function ($noty) {
                        $.ajax({
                            url: href,
                            type: 'post',
                            dataType: 'json'
                        }).done(function (r) {
                            $noty.close();
                            if (!r.error) {
                                $('#jtable').jtable('load');
                                noty({text: r.msg, type: 'success', timeout: 5000});
                            } else {
                                noty({text: r.msg, type: 'error', timeout: 5000});
                            }
                        });
                    }
                },
                {addClass: 'btn btn-danger', text: 'Não', onClick: function ($noty) {
                        $noty.close();
                        noty({text: 'Você optou por não excluir esse registro.', type: 'warning', timeout: 5000});
                    }
                }
            ]
        });
    });

    $.unblockUI();
});