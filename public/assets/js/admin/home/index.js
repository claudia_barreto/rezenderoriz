// prepare the form when the DOM is ready 
$(document).ready(function () {
    $('.thmb').hover(function () {
        var t = jQuery(this);
        t.find('.ckbox').show();
        t.find('.fm-group').show();
    }, function () {

        var t = jQuery(this);
        if (!t.closest('.thmb').hasClass('checked')) {
            t.find('.ckbox').hide();
            t.find('.fm-group').hide();
        }
    });

    $('.ckbox').each(function () {
        var t = jQuery(this);
        var parent = t.parent();
        if (t.find('input').is(':checked')) {
            t.show();
            parent.find('.fm-group').show();
            parent.addClass('checked');
        }
    });

    $('.ckbox').click(function () {
        var t = jQuery(this);
        if (!t.find('input').is(':checked')) {
            t.closest('.thmb').removeClass('checked');
            enable_itemopt(false);
        } else {
            t.closest('.thmb').addClass('checked');
            enable_itemopt(true);
        }
    });

    $("a.delete-images").on('click', function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        var div = $(this).parents('div.document').eq(0);
        noty({
            layout: 'center',
            modal: true,
            text: 'Deseja realmente excluir a imagem?',
            buttons: [
                {addClass: 'btn btn-primary', text: 'Sim', onClick: function ($noty) {
                        $.ajax({
                            url: href,
                            type: 'post',
                            dataType: 'json'
                        }).done(function (r) {
                            $noty.close();
                            if (!r.error) {
                                div.remove();
                                noty({text: r.msg, type: 'success', timeout: 5000});
                            } else {
                                noty({text: r.msg, type: 'error', timeout: 5000});
                            }
                        });
                    }
                },
                {addClass: 'btn btn-danger', text: 'Não', onClick: function ($noty) {
                        $noty.close();
                        noty({text: 'Você optou por não excluir a imagem.', type: 'warning', timeout: 5000});
                    }
                }
            ]
        });
    });
});

