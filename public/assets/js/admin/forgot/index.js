// prepare the form when the DOM is ready 
$(document).ready(function () {
    var options = {
        success: showResponse, // post-submit callback
        type: 'post', // 'get' or 'post', override for form's 'method' attribute
        dataType: 'json' // 'xml', 'script', or 'json' (expected server response type)
    };
    // bind to the form's submit event 
    $('#form-forgot').submit(function () {
        $(this).ajaxSubmit(options);
        return false;
    });
});

// post-submit callback 
function showResponse(responseText, statusText, xhr, $form) {
    if (responseText.error) {
        noty({
            layout: 'center',
            modal: true,
            text: responseText.msg,
            buttons: [
                {
                    addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                        $noty.close();
                    }
                }
            ]
        });
    } else {
        location.reload();
    }
} 