
$(document).ready(function () {

    $('#jtable').jtable({
        title: 'Listagem',
        paging: true,
        pageSize: 10,
        sorting: true,
        defaultSorting: 'id DESC',
        actions: {
            listAction: window.location.href + "/listagem"
        },
        fields: {
            btnEdit: {
                width: '1',
                sorting: false,
                display: function (data) {
                    return $("<a title='Editar' href='" + window.location.href + "/editar/" + data.record.id + "'><i class='glyphicon glyphicon-edit'></i></a>");
                }
            },
            btnDelete: {
                width: '1',
                sorting: false,
                display: function (data) {
                    return $("<a title='Excluir' class='delete' href='" + window.location.href + "/excluir/" + data.record.id + "'><i class='glyphicon glyphicon-trash'></i></a>");
                }
            },
            id: {
                title: 'Código',
                key: true,
                width: '1',
                list: true
            },
            avatar: {
                title: 'Foto',
                width: '10%',
                display: function (data) {
                    return $("<img width='60' src='" + data.record.photo + "'/>");
                }
            },
            name: {
                title: 'Nome',
                width: '25%'
            },
            email: {
                title: 'E-mail',
                width: '25%'
            },
            username: {
                title: 'Usuário',
                width: '15%'
            },
            group: {
                title: 'Grupo',
                width: '25%',
                display: function (data) {
                    return data.record.group.name;
                }
            }
        }
    });
    $('#jtable').jtable('load');

    $('#btnSearch').click(function (e) {
        e.preventDefault();
        $('#jtable').jtable('load', $("#formSearch").serialize());
    });
    $('#btnClean').click(function () {
        $('#jtable').jtable('load');
    });

});
