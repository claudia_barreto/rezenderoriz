// prepare the form when the DOM is ready 
$(document).ready(function () {
    var options = {
        success: showResponse, // post-submit callback
        type: 'post', // 'get' or 'post', override for form's 'method' attribute
        dataType: 'json' // 'xml', 'script', or 'json' (expected server response type)
    };
    // bind to the form's submit event 
    $('#form').submit(function () {
        $(this).ajaxSubmit(options);
        return false;
    });

    $("input#avatar").change(function () {
        readURL(this);
    });
});

// post-submit callback 
function showResponse(responseText, statusText, xhr, $form) {
    noty({
        layout: 'center',
        modal: true,
        text: responseText.msg,
        buttons: [
            {
                addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                    $noty.close();
                    if (!responseText.error) {
                        if (document.getElementById('id')) {
                            window.location = window.url + "/projetos";
                        } else {
                            document.getElementById('form').reset();
                        }
                    }
                }
            }
        ]
    });
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#preview-photo').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
