
$(document).ready(function () {

    $('#jtable').jtable({
        title: 'Listagem',
        paging: true,
        pageSize: 10,
        sorting: true,
        defaultSorting: 'id DESC',
        actions: {
            listAction: window.location.href + "/listagem"
        },
        fields: {
            btnEdit: {
                width: '1',
                sorting: false,
                display: function (data) {
                    return $("<a title='Editar' href='" + window.location.href + "/editar/" + data.record.id + "'><i class='glyphicon glyphicon-edit'></i></a>");
                }
            },
            btnDelete: {
                width: '1',
                sorting: false,
                display: function (data) {
                    return $("<a title='Excluir' class='delete' href='" + window.location.href + "/excluir/" + data.record.id + "'><i class='glyphicon glyphicon-trash'></i></a>");
                }
            },
            id: {
                title: 'Código',
                key: true,
                width: '1',
                list: true
            },
            name: {
                title: 'Nome',
                width: '20%'
            },
            category: {
                title: 'Categoria',
                width: '20%',
                display: function (data) {
                    if (data.record.category) {
                        return data.record.category.title;
                    }
                }
            },
            neighborhood: {
                title: 'Bairro',
                width: '20%'
            },
            city: {
                title: 'Cidade',
                width: '20%'
            },
            room: {
                title: 'Quartos',
                width: '20%'
            }
        }
    });
    $('#jtable').jtable('load');

    $('#btnSearch').click(function (e) {
        e.preventDefault();
        $('#jtable').jtable('load', $("#formSearch").serialize());
    });
    $('#btnClean').click(function () {
        $('#jtable').jtable('load');
    });

});
