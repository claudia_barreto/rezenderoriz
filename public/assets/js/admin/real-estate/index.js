// prepare the form when the DOM is ready 
$(document).ready(function () {

    $('.thmb').hover(function () {
        var t = jQuery(this);
        t.find('.ckbox').show();
        t.find('.fm-group').show();
    }, function () {

        var t = jQuery(this);
        if (!t.closest('.thmb').hasClass('checked')) {
            t.find('.ckbox').hide();
            t.find('.fm-group').hide();
        }
    });

    $('.ckbox').each(function () {
        var t = jQuery(this);
        var parent = t.parent();
        if (t.find('input').is(':checked')) {
            t.show();
            parent.find('.fm-group').show();
            parent.addClass('checked');
        }
    });

    $('.ckbox').click(function () {
        var t = jQuery(this);
        if (!t.find('input').is(':checked')) {
            t.closest('.thmb').removeClass('checked');
            enable_itemopt(false);
        } else {
            t.closest('.thmb').addClass('checked');
            enable_itemopt(true);
        }
    });

    $("a.save-updates").on('click', function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        var div = $(this).parents('div.document').eq(0);
        $.ajax({
            url: href,
            type: 'post',
            dataType: 'json',
            data: {
                description: div.find("textarea#description_plants2").val()
            }
        }).done(function (r) {
            if (!r.error) {
                noty({text: r.msg, type: 'success', timeout: 5000});
            } else {
                noty({text: r.msg, type: 'error', timeout: 5000});
            }
        });
    });

    $("a.delete-images").on('click', function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        var div = $(this).parents('div.document').eq(0);
        noty({
            layout: 'center',
            modal: true,
            text: 'Deseja realmente excluir a imagem?',
            buttons: [
                {addClass: 'btn btn-primary', text: 'Sim', onClick: function ($noty) {
                        $.ajax({
                            url: href,
                            type: 'post',
                            dataType: 'json'
                        }).done(function (r) {
                            $noty.close();
                            if (!r.error) {
                                div.remove();
                                noty({text: r.msg, type: 'success', timeout: 5000});
                            } else {
                                noty({text: r.msg, type: 'error', timeout: 5000});
                            }
                        });
                    }
                },
                {addClass: 'btn btn-danger', text: 'Não', onClick: function ($noty) {
                        $noty.close();
                        noty({text: 'Você optou por não excluir a imagem.', type: 'warning', timeout: 5000});
                    }
                }
            ]
        });
    });

    deleteInvestment();

    $("button#add-investment").on('click', function (e) {
        e.preventDefault();
        $("div#list-investment").append(htmlInvestment());
        deleteInvestment();
    });

});

function htmlInvestment() {
    var html = "";
    html += '<div class="form-group">';
    html += '    <label class="col-sm-2 control-label" for="description_i">Descrição</label>';
    html += '    <div class="col-sm-4">';
    html += '        <input type="text" id="description_i" name="description_i[]" class="form-control" placeholder="exemplo: 1 x R$ 600.000,00 = R$600.000,00">';
    html += '    </div>';
    html += '    <div class="col-sm-1">';
    html += '        <button href="#" class="btn btn-danger delete-investment"><i class="glyphicon glyphicon-plus"></i> Excluir</button>';
    html += '    </div>';
    html += '</div>';

    return html;
}

function deleteInvestment() {
    $("button.delete-investment").off('click').on('click', function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        var div = $(this).parents('div.form-group').eq(0);
        if (href != '#') {
            noty({
                layout: 'center',
                modal: true,
                text: 'Deseja realmente excluir o registro?',
                buttons: [
                    {addClass: 'btn btn-primary', text: 'Sim', onClick: function ($noty) {
                            $.ajax({
                                url: href,
                                type: 'post',
                                dataType: 'json'
                            }).done(function (r) {
                                $noty.close();
                                if (!r.error) {
                                    div.remove();
                                    noty({text: r.msg, type: 'success', timeout: 5000});
                                } else {
                                    noty({text: r.msg, type: 'error', timeout: 5000});
                                }
                            });
                        }
                    },
                    {addClass: 'btn btn-danger', text: 'Não', onClick: function ($noty) {
                            $noty.close();
                            noty({text: 'Você optou por não excluir o registro.', type: 'warning', timeout: 5000});
                        }
                    }
                ]
            });
        } else {
            div.remove();
        }
    });
}