// prepare the form when the DOM is ready 
$(document).ready(function () {
    var options = {
        beforeSubmit: function () {
            $("#loadRezende").css({'display': 'block'});
        },
        success: showResponse, // post-submit callback
        type: 'post', // 'get' or 'post', override for form's 'method' attribute
        dataType: 'json' // 'xml', 'script', or 'json' (expected server response type)
    };
    // bind to the form's submit event 
    $('#frm').submit(function () {
        $(this).ajaxSubmit(options);
        return false;
    });
});

// post-submit callback 
function showResponse(responseText, statusText, xhr, $form) {
    noty({
        layout: 'center',
        modal: true,
        text: responseText.msg,
        buttons: [
            {
                addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                    $noty.close();
                    $("#loadRezende").css({'display': 'none'});
                    if (!responseText.error) {
                        document.getElementById('frm').reset();
                    }
                }
            }
        ]
    });
}
