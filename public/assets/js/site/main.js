//Dropdown        
        var scale = $('body').width();
        var mobile = 767;
        if (scale > mobile ) {
             $(function(){
                $(".dropdown").hover(            
                        function() {
                            $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                            $(this).toggleClass('open');               
                        },
                        function() {
                            $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                            $(this).toggleClass('open');               
                        });
            });            
        }else{
            $(".dropdown-toggle").removeClass('dropdown-toggle');
            $(".dropdown a").removeAttr('data-toggle');
        }

